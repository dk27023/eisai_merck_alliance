drop table if exists eisai_prod_db.stg_spec_pharm;
create table eisai_prod_db.stg_spec_pharm AS
select 
CNTRY_ID
,PRODUCT_LEVEL
,PRODUCT_ID
,PRODUCT_NAME
,indication
,time_period
,to_date(time_period_start, 'YYYY-MM-DD', FALSE) as time_period_start
,to_date(time_period_end, 'YYYY-MM-DD', FALSE) as time_period_end
,therapy_type
,specialty_pharmacy_code
,time_to_first_fill
,time_to_first_fill_change
,fill_rate
,fill_rate_change
,time_on_therapy
,time_on_therapy_change
,active_patients
,new_patients
,DISCONTINUED_patients
,abandonment_patients
,copay_redemption_patient
,to_date(DATA_DLVRY_DATE, 'DDMONYYYY', FALSE) as DATA_DLVRY_DATE
,to_date(DATA_RCVD_DATE, 'DDMONYYYY', FALSE) as DATA_RCVD_DATE
from eisai_prod_db.ldg_spec_pharm;



select count(*) from eisai_prod_db.stg_spec_pharm;
select * from eisai_prod_db.stg_spec_pharm;
