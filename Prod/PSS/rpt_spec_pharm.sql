drop table if exists eisai_prod_db.rpt_spec_pharm;
create table eisai_prod_db.rpt_spec_pharm AS

select * from
(
--'C13W', 'C4W', 'CM', 'CW', 'MTD'
--'C12M', 'YTD', 'QTD'

select cntry_id,product_level,product_id,product_name, stg.indication,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date, 
d_time_bckt.time_bckt_freq, d_time_bckt.time_bckt_strt, d_time_bckt.time_bckt_end
,d_time_bckt.time_period_mod as time_bckt_cd
FROM

(select 
cntry_id,product_level,product_id,product_name,indication, time_period, time_period_start,time_period_end,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date
from eisai_prod_db.stg_spec_pharm
where UPPER(trim(time_period)) in ('C13W', 'C4W', 'CM', 'CW', 'MTD', 'C12M', 'YTD', 'QTD')
) stg


left join

(SELECT distinct indication,time_bckt_strt,time_bckt_end
,CASE WHEN time_bckt_cd='R13W' THEN 'C13W'
WHEN time_bckt_cd='R4W' THEN 'C4W'
WHEN time_bckt_cd='M1' THEN 'CM'
WHEN time_bckt_cd='WTD' THEN 'CW'
WHEN time_bckt_cd='R12M' THEN 'C12M'
WHEN time_bckt_cd='YTD (W)' THEN 'YTD'
WHEN time_bckt_cd='QTD1 (W)' THEN 'QTD'
ELSE time_bckt_cd END AS time_period_mod
,time_bckt_freq
 from eisai_prod_db.d_time_bckt where UPPER(TRIM(source)) = 'SALES' ) d_time_bckt
ON
UPPER(trim(stg.time_period)) = UPPER(trim(d_time_bckt.time_period_mod)) 
and stg.time_period_end = d_time_bckt.time_bckt_end and stg.time_period_start = d_time_bckt.time_bckt_strt




--MONTHLY, WEEKLY
UNION ALL

select cntry_id,product_level,product_id,product_name, stg.indication,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date, 
d_time_bckt.time_bckt_freq, d_time_bckt.time_bckt_strt, d_time_bckt.time_bckt_end
,time_bckt_cd
FROM

(select 
cntry_id,product_level,product_id,product_name,indication, time_period, time_period_start,time_period_end,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date
from eisai_prod_db.stg_spec_pharm
where UPPER(trim(time_period)) in ('MONTHLY', 'WEEKLY') 
) stg

inner join

(SELECT distinct indication,time_bckt_strt,time_bckt_end
,time_bckt_cd
,time_bckt_freq
from eisai_prod_db.d_time_bckt where UPPER(TRIM(source)) = 'SALES' ) d_time_bckt
ON
UPPER(trim(stg.time_period)) = UPPER(trim(d_time_bckt.time_bckt_freq)) 
and stg.time_period_start    = d_time_bckt.time_bckt_strt
and stg.time_period_end      = d_time_bckt.time_bckt_end


--LTD
UNION ALL

select cntry_id,product_level,product_id,product_name, stg.indication,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date, 
d_time_bckt.time_bckt_freq, d_time_bckt.time_bckt_strt, d_time_bckt.time_bckt_end
,d_time_bckt.time_period_mod as time_bckt_cd
FROM

(select 
cntry_id,product_level,product_id,product_name,indication, time_period, time_period_start,time_period_end,therapy_type,specialty_pharmacy_code,time_to_first_fill,time_to_first_fill_change,fill_rate,fill_rate_change,time_on_therapy,time_on_therapy_change,active_patients,new_patients,discontinued_patients,abandonment_patients,copay_redemption_patient,data_dlvry_date,data_rcvd_date
from eisai_prod_db.stg_spec_pharm
where UPPER(trim(time_period)) = 'LTD'
) stg

left join

(SELECT distinct indication,time_bckt_strt,time_bckt_end
,CASE WHEN time_bckt_cd='LTD (W)' THEN 'LTD'
ELSE time_bckt_cd END AS time_period_mod
,time_bckt_freq
 from eisai_prod_db.d_time_bckt where UPPER(TRIM(source)) = 'SALES' ) d_time_bckt
ON
UPPER(trim(stg.time_period)) = UPPER(trim(d_time_bckt.time_period_mod)) 
and stg.time_period_start = d_time_bckt.time_bckt_strt 
and UPPER(trim(stg.indication)) = UPPER(trim(d_time_bckt.indication))

) ;

