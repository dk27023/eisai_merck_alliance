TRUNCATE table eisai_prod_db.ldg_spec_pharm;

copy eisai_prod_db.ldg_spec_pharm 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/PSS/Speciality_Pharmacy/specialty_pharmacy.txt' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
removequotes
delimiter '|'
IGNOREHEADER 1
;