create table eisai_prod_db.i_button_metric_spec_phar as 
select distinct 
'Time to First Fill' as metric, 'It is the difference between the first ship date and the referral date for each patient' as description
union 
select distinct 'Fill Rate', 'Ratio of number of patients shipped to new patients referred'
union select distinct 'Time on Therapy', 'The time a patient has been on therapy'
union select distinct 'Copay Redemption %',' Copay redeemed over total'
union select distinct 'Total Patients', 'Total number of Patients for that indication and time period'
union select distinct 'New Patients', 'Total number of New Patients for that indication and time period'
union select distinct 'Discontinued Patients', 'Total number of Discontinued Patients for that indication and time period'
union select distinct 'Abandonment Patients', 'Total number of Abandoned Patients for that indication and time period';
