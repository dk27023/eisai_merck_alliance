drop table if exists eisai_prod_db.stg_calls_category;
create table eisai_prod_db.stg_calls_category AS

SELECT distinct
calls_cat.CNTRY_ID                                                                                 
,calls_cat.PRODUCT_ID                                                                              
,calls_cat.PRODUCT_NAME                                                                                 
,calls_cat.BRAND_ID                                                                                 
,calls_cat.BRAND_NAME                                                                                
,calls_cat.indication                                                                           
,calls_cat.SPECIALTY_PHARMACY_CODE         
,calls_cat.THERAPY_TYPE
,calls_cat.TIME_PERIOD
,TO_DATE(calls_cat.TIME_PERIOD_START, 'YYYY-MM-DD') as TIME_PERIOD_START
,TO_DATE(calls_cat.TIME_PERIOD_END, 'YYYY-MM-DD') as TIME_PERIOD_END
,COALESCE(spec_phar.time_bckt_cd, null) as time_bckt_cd
,calls_cat.CALL_CATEGORY                
,calls_cat.CALLS
,TO_DATE(calls_cat.DATA_DLVRY_DATE, 'YYYY-MM-DD') as DATA_DLVRY_DATE
,TO_DATE(calls_cat.DATA_RCVD_DATE, 'YYYY-MM-DD') as DATA_RCVD_DATE
FROM eisai_prod_db.ldg_calls_category as calls_cat

left join (select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.rpt_spec_pharm where time_bckt_cd <> 'CW' and time_bckt_cd <> 'CM') as spec_phar
on calls_cat.TIME_PERIOD_START = spec_phar.time_bckt_strt
and calls_cat.TIME_PERIOD_END = spec_phar.time_bckt_end;
