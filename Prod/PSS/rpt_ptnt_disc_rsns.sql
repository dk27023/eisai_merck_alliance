DROP TABLE IF EXISTS eisai_prod_db.rpt_ptnt_disc_rsns;
CREATE TABLE eisai_prod_db.rpt_ptnt_disc_rsns AS 
SELECT pd.cntry_id,
pd.product_id,
pd.product_name,
pd.brand_id,
pd.brand_name,
pd.indication,
pd.therapy_type,
pd.time_period,
pd.discontinuation_reason,
pd.discontinuation_patients,
tb.time_bckt_strt,
tb.time_bckt_end
FROM eisai_prod_db.ldg_ptnt_disc_rsns pd
LEFT JOIN
(
SELECT distinct indication,time_bckt_strt,time_bckt_end
,CASE WHEN time_bckt_cd='R13W' THEN 'C13W'
WHEN time_bckt_cd='R4W' THEN 'C4W'
WHEN time_bckt_cd='R12M' THEN 'C12M'
WHEN time_bckt_cd='M1' THEN 'CM'
WHEN time_bckt_cd='WTD' THEN 'CW'
WHEN time_bckt_cd='LTD (W)' THEN 'LTD'
WHEN time_bckt_cd='YTD (W)' THEN 'YTD'
WHEN time_bckt_cd='QTD1 (W)' THEN 'QTD'
 ELSE time_bckt_cd END AS time_period_mod
FROM eisai_prod_db.d_time_bckt where  upper(trim(source)) ='SALES'
) as tb
on (tb.time_period_mod=pd.time_period and time_period_mod<>'LTD') 
or (tb.time_period_mod=pd.time_period and time_period_mod='LTD' and tb.indication=pd.indication);

