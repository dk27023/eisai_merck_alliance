drop table if exists eisai_prod_db.i_button_time_bckt_pss ;
create table  eisai_prod_db.i_button_time_bckt_pss as 

select distinct 'ANY' as indication, 'CM' as time_bckt, 'Current Full Month' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'CM'

UNION ALL

select distinct 'ANY' as indication,'CW' as time_bckt, 'Currrent Week' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'CW'

UNION ALL

select distinct 'ANY' as indication,'C12M' as time_bckt, 'Rolling 12 Months' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'C12M'

UNION ALL

select distinct 'ANY' as indication,'C13W' as time_bckt, 'Rolling 13 Weeks' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'C13W'

UNION ALL

select distinct 'ANY' as indication,'C4W' as time_bckt, 'Rolling 4 Weeks' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'C4W'


UNION ALL

select distinct 'ANY' as indication,'MTD' as time_bckt, 'Month to Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'MTD'

UNION ALL

select distinct 'ANY' as indication,'QTD' as time_bckt, 'Quarter to Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'QTD'

UNION ALL

select distinct 'ANY' as indication,'YTD' as time_bckt, 'Year to Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'YTD'

UNION ALL

select distinct indication,'LTD' as time_bckt, 'Launch to Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_spec_pharm where time_bckt_cd = 'LTD'
;


