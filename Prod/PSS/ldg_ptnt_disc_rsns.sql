truncate table eisai_prod_db.ldg_ptnt_disc_rsns;

copy eisai_prod_db.ldg_ptnt_disc_rsns 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/PSS/Patient_Discontinuation/ptnt_disctn_rsns.txt' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
removequotes
delimiter '|'
IGNOREHEADER 1
;