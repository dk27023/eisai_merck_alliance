drop table if exists eisai_prod_db.ctl_job_run;
create table eisai_prod_db.ctl_job_run as

select * from

(select * from 
(SELECT
'1' as run_id, activity_feed as var_val, activity_feed
,concat( concat ( substring(activity_feed,1,4), substring(activity_feed,6,2) ) , substring(activity_feed,9,2) )as cycl_time_id
FROM
(select max(to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) ) as activity_feed from eisai_prod_db.ldg_call_kpi_eisai) activity
) fnl_one
join
(select max(to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) ) as sales_feed from eisai_prod_db.ldg_sales_kpi_eisai) sales
on 1=1
) fnl_two
join
(select distinct curr_mth_end as finance_feed from eisai_dev_db.trx_tm_tbl) finance
on 1=1
join
(select max(TIME_BCKT_END) as Performance_summary_feed from eisai_dev_db.rpt_iqvia_report) performance_summary
on 1=1
join
(select max(TIME_BCKT_END) as SP_summary_feed from eisai_dev_db.rpt_spec_pharm) SP_summary
on 1=1
join
(select max(TIME_PERIOD_END) as Digital_summary_feed from eisai_dev_db.stg_digital_mkt) digital_summary
on 1=1
join
(select max(time_bckt_end) as Productivity_report_feed from eisai_dev_db.rpt_prod_rpt_sales) productivity_report
on 1=1;