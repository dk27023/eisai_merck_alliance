drop table if exists eisai_prod_db.refresh_date_tbl;
create table eisai_prod_db.refresh_date_tbl as 

select 1 as Display_Id, 'Report Refresh Date' as Display_desc, CURRENT_DATE as Display_Date
UNION
select 2, 'Sales Date', to_date(sales_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run
UNION
select 3, 'Activity Date', to_date(activity_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run
UNION 
select 4, 'Finance Month Ending Date', to_date(finance_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run
UNION
select 5, 'Performance Summary Date', to_date(performance_summary_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run
UNION
select 6, 'Speciality Pharmacy Month Ending Date', SP_summary_feed from eisai_prod_db.ctl_job_run
UNION
select 7, 'Digital Marketing Date', to_date(Digital_summary_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run
UNION
select 8, 'Productivity Report Date', to_date(Productivity_report_feed, 'YYYY-MM-DD') from eisai_prod_db.ctl_job_run;