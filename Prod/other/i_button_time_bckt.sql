drop table if exists eisai_prod_db.i_button_time_bckt ;
create table  eisai_prod_db.i_button_time_bckt as 

select distinct indication,'LTD (Launch To Date)' as time_bckt, 'Launch Date for Indication from Prescriber Sales data set' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'LTD (W)' and upper(trim(source)) = 'SALES'
union 
select distinct indication,'QTD (Quarter To Date)' as time_bckt, 'Start of Quarter to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'QTD1 (W)' and upper(trim(source)) = 'SALES'
union
select distinct indication,'YTD (Year To Date)' as time_bckt, 'Start of Year to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'YTD (W)' and upper(trim(source)) = 'SALES'
union
select distinct indication,'MTD (Month To Date)' as time_bckt, 'Start of Month to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'MTD' and upper(trim(source)) = 'SALES'
union
select distinct indication,'R12M (Rolling 12 Months)' as time_bckt, 'Complete 12 Months' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R12M' and upper(trim(source)) = 'SALES'
union
select distinct indication,'CM (Current Month)' as time_bckt, 'Complete 1 Month' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'M1' and upper(trim(source)) = 'SALES'
union
select distinct indication,'R4W (Rolling 4 Weeks)' as time_bckt, 'The 4 weeks leading up to data refresh week i.e W4 - W1' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R4W' and upper(trim(source)) = 'SALES'
union
select distinct indication,'R13W (Rolling 13 Weeks)' as time_bckt, 'The 13 weeks leading up to data refresh week i.e W13 - W1' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R13W' and upper(trim(source)) = 'SALES'
union
select distinct indication,'CW (Current Week)' as time_bckt, 'Complete week' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'WTD' and upper(trim(source)) = 'SALES'
union
select distinct indication,'FYTD (Fiscal Year To Date)' as time_bckt, 'Fiscal Year To Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Sales' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'FYTD1 (W)' and upper(trim(source)) = 'SALES'

union

select distinct indication,'LTD (Launch To Date)' as time_bckt, 'Launch Date for Indication from Prescriber Sales data set' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'LTD (W)' and upper(trim(source)) = 'ACTIVITY'
union 
select distinct indication,'QTD (Quarter To Date)' as time_bckt, 'Start of Quarter to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'QTD1 (W)' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'YTD (Year To Date)' as time_bckt, 'Start of Year to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'YTD (W)' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'MTD (Month To Date)' as time_bckt, 'Start of Month to Current Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'MTD' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'R12M (Rolling 12 Months)' as time_bckt, 'Complete 12 Months' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R12M' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'CM (Current Month)' as time_bckt, 'Complete 1 Month' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'M1' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'R4W (Rolling 4 Weeks)' as time_bckt, 'The 4 weeks leading up to data refresh week i.e W4 - W1' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R4W' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'R13W (Rolling 13 Weeks)' as time_bckt, 'The 13 weeks leading up to data refresh week i.e W13 - W1' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'R13W' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'CW (Current Week)' as time_bckt, 'Complete week' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'WTD' and upper(trim(source)) = 'ACTIVITY'
union
select distinct indication,'FYTD (Fiscal Year To Date)' as time_bckt, 'Fiscal Year To Date' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end, 'Activity' as src from eisai_prod_db.d_time_bckt where time_bckt_cd = 'FYTD1 (W)' and upper(trim(source)) = 'ACTIVITY'
;
