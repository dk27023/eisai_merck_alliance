drop table if exists eisai_prod_db.f_call_goals;
create table eisai_prod_db.f_call_goals AS

(
--'FYTD1 (W)'

select distinct source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication , src_indication,
sum(call_goals_cal) over (partition by source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication,src_indication) as call_goals_prorated
from
(select goals.*
,case when  (qtr_time_bckt_strt BETWEEN FYTD_time_bckt_strt and time_bckt_end) AND (qtr_time_bckt_end BETWEEN FYTD_time_bckt_strt and time_bckt_end) then call_goals
when (qtr_time_bckt_strt BETWEEN FYTD_time_bckt_strt and time_bckt_end) then (1+datediff(day, qtr_time_bckt_strt, time_bckt_end))*call_goals/day_diff
else 0 end as call_goals_cal

from
(select time_bckts.*, stg.*
,1+datediff(day, to_date(qtr_time_bckt_strt,'YYYY-MM-DD'), to_date(qtr_time_bckt_end,'YYYY-MM-DD') )as day_diff 
FROM
(select organization, fiscal_year, quarter, grouped_indication , indication as src_indication, call_goals,
 to_date(qtr_time_bckt_strt,'YYYY-MM-DD') as qtr_time_bckt_strt,
 to_date(qtr_time_bckt_end,'YYYY-MM-DD') as qtr_time_bckt_end from eisai_prod_db.stg_call_goals_all) stg

join

(
select all_time_bckt.*, FYTD_time_bckt_strt from
(
select *
from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ('FYTD1 (W)')
) as all_time_bckt
join
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) time_bckts
on 1=1
) goals
) prorated



--'WTD', 'R4W'
UNION ALL

select source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication , src_indication,
sum(call_goals_cal) over (partition by source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication,src_indication) as call_goals_prorated
from
(select goals.*
,case when  (time_bckt_strt BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) AND (time_bckt_end BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) then (1+datediff(day, time_bckt_strt, time_bckt_end))*call_goals/day_diff

when (time_bckt_strt BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) then (1+datediff(day, time_bckt_strt, qtr_time_bckt_end))*call_goals/day_diff
when (time_bckt_end BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) then (1+datediff(day, qtr_time_bckt_strt, time_bckt_end))*call_goals/day_diff
else 0 end as call_goals_cal

from
(select time_bckts.*, stg.*
,1+datediff(day, to_date(qtr_time_bckt_strt,'YYYY-MM-DD'), to_date(qtr_time_bckt_end,'YYYY-MM-DD') )as day_diff 
FROM
(select organization, fiscal_year, quarter, grouped_indication , indication as src_indication, call_goals,
 to_date(qtr_time_bckt_strt,'YYYY-MM-DD') as qtr_time_bckt_strt,
 to_date(qtr_time_bckt_end,'YYYY-MM-DD') as qtr_time_bckt_end from eisai_prod_db.stg_call_goals_all) stg

join

(
select all_time_bckt.*, FYTD_time_bckt_strt from
(
select *
from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ('WTD', 'R4W')
) as all_time_bckt
join
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) time_bckts
on 1=1
) goals
) prorated



--'M1', 'MTD', 'QTD1 (W)'
UNION ALL

select source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication , src_indication,
sum(call_goals_cal) over (partition by source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication,src_indication) as call_goals_prorated
from
(select goals.*
,case when (time_bckt_strt BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) AND (time_bckt_end BETWEEN qtr_time_bckt_strt and qtr_time_bckt_end) then (1+datediff(day, time_bckt_strt, time_bckt_end))*call_goals/day_diff

else 0 end as call_goals_cal

from
(select time_bckts.*, stg.*
,1+datediff(day, to_date(qtr_time_bckt_strt,'YYYY-MM-DD'), to_date(qtr_time_bckt_end,'YYYY-MM-DD') )as day_diff 
FROM
(select organization, fiscal_year, quarter, grouped_indication , indication as src_indication, call_goals,
 to_date(qtr_time_bckt_strt,'YYYY-MM-DD') as qtr_time_bckt_strt,
 to_date(qtr_time_bckt_end,'YYYY-MM-DD') as qtr_time_bckt_end from eisai_prod_db.stg_call_goals_all) stg

join

(
select all_time_bckt.*, FYTD_time_bckt_strt from
(
select *
from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ('M1', 'MTD', 'QTD1 (W)')
) as all_time_bckt
join
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) time_bckts
on 1=1
) goals
) prorated




--'YTD (W)', 'R12M', 'R13W'  (3 quarter case also covered)
UNION ALL

select source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication , src_indication,
sum(call_goals_cal) over (partition by source, time_bckt_freq, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, fytd_time_bckt_strt, organization, fiscal_year, grouped_indication,src_indication) as call_goals_prorated
from
(select goals.*
,case when (qtr_time_bckt_strt BETWEEN	time_bckt_strt and time_bckt_end) AND (qtr_time_bckt_end BETWEEN time_bckt_strt and time_bckt_end) then call_goals
when (qtr_time_bckt_strt BETWEEN time_bckt_strt and time_bckt_end) then (1+datediff(day, qtr_time_bckt_strt, time_bckt_end))*call_goals/day_diff
when (qtr_time_bckt_end BETWEEN time_bckt_strt and time_bckt_end) then (1+datediff(day, time_bckt_strt, qtr_time_bckt_end))*call_goals/day_diff
else 0 end as call_goals_cal

from
(select time_bckts.*, stg.*
,1+datediff(day, to_date(qtr_time_bckt_strt,'YYYY-MM-DD'), to_date(qtr_time_bckt_end,'YYYY-MM-DD') )as day_diff 
FROM
(select organization, fiscal_year, quarter, grouped_indication , indication as src_indication, call_goals,
 to_date(qtr_time_bckt_strt,'YYYY-MM-DD') as qtr_time_bckt_strt,
 to_date(qtr_time_bckt_end,'YYYY-MM-DD') as qtr_time_bckt_end from eisai_prod_db.stg_call_goals_all) stg

join

(
select all_time_bckt.*, FYTD_time_bckt_strt from
(
select *
from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ('R13W', 'R12M', 'YTD (W)')
) as all_time_bckt
join
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_prod_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) time_bckts
on 1=1
) goals
) prorated


);



