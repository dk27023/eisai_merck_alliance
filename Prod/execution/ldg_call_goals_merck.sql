drop table if exists eisai_prod_db.ldg_call_goals_merck;
create table eisai_prod_db.ldg_call_goals_merck
(                                                                           
ORGANIZATION            varchar(255)                                                                              
,YEAR               	    varchar(255)                                                                                 
,QUARTER                      varchar(255)                                                                                 
,INDICATION                		varchar(255)                                                                                 
,CALL_GOALS                     varchar(255)                                                                                                          
);

TRUNCATE table eisai_prod_db.ldg_call_goals_merck;

copy eisai_prod_db.ldg_call_goals_merck 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Merck_Goals/call_goals_merck.txt' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
removequotes
delimiter '\t'
IGNOREHEADER 1
;

select * from eisai_prod_db.ldg_call_goals_merck;
