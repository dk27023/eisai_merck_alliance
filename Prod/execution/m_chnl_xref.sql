drop table if exists eisai_prod_db.m_chnl_xref;
create table if not exists eisai_prod_db.m_chnl_xref
(EISAI_CALL_CHANNEL VARCHAR(255)
,MERCK_CALL_CHANNEL VARCHAR(255)
,CALL_CHANNEL VARCHAR(255)
) 

copy eisai_prod_db.m_chnl_xref 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/in/ZS_Soure_Tables/Call_channel_ref_v2.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;