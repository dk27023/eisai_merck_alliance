drop table if exists eisai_prod_db.stg_call_goals_eisai;
create table eisai_prod_db.stg_call_goals_eisai AS

select fnl.* , i_xref.grouped_indication
from
(select ORGANIZATION
,YEAR as FISCAL_YEAR
,QUARTER
,xref.GROUPED_INDICATION as indication
,case when quarter = 'Q1' then CONCAT(FISCAL_YEAR,'-04-01')
when quarter = 'Q2' then CONCAT(FISCAL_YEAR,'-07-01')
when quarter = 'Q3' then CONCAT(FISCAL_YEAR,'-10-01')
when quarter = 'Q4' then CONCAT(cast(FISCAL_YEAR as int)+1,'-01-01')
else 'NA' end as qtr_time_bckt_strt

,case when quarter = 'Q1' then CONCAT(FISCAL_YEAR,'-06-30')
when quarter = 'Q2' then CONCAT(FISCAL_YEAR,'-09-30')
when quarter = 'Q3' then CONCAT(FISCAL_YEAR,'-12-31')
when quarter = 'Q4' then CONCAT(cast(FISCAL_YEAR as  int)+1,'-03-31')
else 'NA' end as qtr_time_bckt_end
,sum(call_goals) as call_goals
from eisai_prod_db.ldg_call_goals_eisai as ldg
left join eisai_prod_db.m_indication_grouping as xref
on upper(trim(ldg.indication)) = upper(trim(xref.indication))
group by 1,2,3,4
)fnl
left join eisai_prod_db.m_indication_grouping as i_xref
on fnl.indication = i_xref.indication
;
