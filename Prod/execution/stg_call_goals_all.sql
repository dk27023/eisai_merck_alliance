drop table if exists eisai_prod_db.stg_call_goals_all;
create table eisai_prod_db.stg_call_goals_all AS

select ORGANIZATION, fiscal_year, quarter, grouped_indication, indication, qtr_time_bckt_strt, qtr_time_bckt_end, call_goals from eisai_prod_db.stg_call_goals_eisai

UNION ALL

select ORGANIZATION, fiscal_year, quarter, grouped_indication, indication, qtr_time_bckt_strt, qtr_time_bckt_end, call_goals from eisai_prod_db.stg_call_goals_merck

UNION ALL

(
SELECT 'Alliance' as ORGANIZATION, fiscal_year, quarter, grouped_indication, indication, qtr_time_bckt_strt, qtr_time_bckt_end, sum(call_goals) as call_goals FROM
(select fiscal_year, quarter, grouped_indication, indication, qtr_time_bckt_strt, qtr_time_bckt_end, call_goals from eisai_prod_db.stg_call_goals_eisai
UNION ALL
select fiscal_year, quarter, grouped_indication, indication, qtr_time_bckt_strt, qtr_time_bckt_end, call_goals from eisai_prod_db.stg_call_goals_merck)
group by 1,2,3,4,5,6,7
);

select * from eisai_prod_db.stg_call_goals_all;
