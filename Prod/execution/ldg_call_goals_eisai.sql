drop table if exists eisai_prod_db.ldg_call_goals_eisai;
create table eisai_prod_db.ldg_call_goals_eisai
(
CNTRY_ID                        varchar(255)                                                                                 
,ORGANIZATION            varchar(255)                                                                              
,YEAR               	    varchar(255)                                                                                 
,QUARTER                      varchar(255)                                                                                 
,INDICATION                		varchar(255)                                                                                 
,CALL_GOALS                     varchar(255)                                                                           
,DATA_DLVRY_DATE              		varchar(255)                                                                          
);

TRUNCATE table eisai_prod_db.ldg_call_goals_eisai;

copy eisai_prod_db.ldg_call_goals_eisai 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Eisai/call_goals_20211105.txt' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
removequotes
delimiter '|'
IGNOREHEADER 1
;

select * from eisai_prod_db.ldg_call_goals_eisai;







