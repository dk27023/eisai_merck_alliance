drop table if exists eisai_prod_db.int_call_activity;
create table eisai_prod_db.int_call_activity as select * from
(
-- Details by Indication for Product Position Details
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
11 as Panel_Id
,'Details by Indication'  as Panel_Name
,cast(concat('11',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as category_id
,indication as category_name
,cast(concat('111',cast(dense_rank() over (partition by null order by product_position) as varchar) )as int) as subcategory_id
,product_position as Subcategory_Name
,11111 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,case when (UPPER(trim(grouped_indication))='ALLIANCE' and UPPER(trim(indication)) = 'ALLIANCE')
	or (UPPER(trim(grouped_indication))='ALL' and UPPER(trim(indication)) = 'ALL')
	then 0 else 1 end as Metric_Val2
,max(target_count) as metric_val3
from eisai_prod_db.idl_call_activity
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,product_position,indication


UNION ALL

-- Details by Indication for Call Channel Details
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
11 as Panel_Id
,'Details by Indication'  as Panel_Name
,cast(concat('11',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as category_id
,indication as category_name
,cast(concat('112',cast(dense_rank() over (partition by null order by mdm_call_channel) as varchar) )as int) as subcategory_id
,mdm_call_channel as Subcategory_Name
,11111 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,case when (UPPER(trim(grouped_indication))='ALLIANCE' and UPPER(trim(indication)) = 'ALLIANCE')
	or (UPPER(trim(grouped_indication))='ALL' and UPPER(trim(indication)) = 'ALL')
	then 0 
	else 1 end as Metric_Val2
,max(target_count) as metric_val3
from eisai_prod_db.idl_call_activity
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,mdm_call_channel,indication


UNION ALL

-- Details by Indication for Target Reach
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
11 as Panel_Id
,'Details by Indication'  as Panel_Name
,cast(concat('11',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as category_id
,indication as category_name
,1131 as subcategory_id
,'Target Reach Indication Percent' as Subcategory_Name
,11112 as Metric_Id
,'Target Reach Ratio By Indication' as Metric_Name
,max(target_reach_ind_ratio) as Metric_Val1
,case when (UPPER(trim(grouped_indication))='ALLIANCE' and UPPER(trim(indication)) = 'ALLIANCE')
	or (UPPER(trim(grouped_indication))='ALL' and UPPER(trim(indication)) = 'ALL')
	then 0 
	else 1 end as Metric_Val2
,max(target_count) as metric_val3
from eisai_prod_db.idl_call_activity
where target_flag = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,indication

UNION ALL

-- Details by Indication for Frequency
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
11 as Panel_Id
,'Details by Indication'  as Panel_Name
,cast(concat('11',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as category_id
,indication as category_name
,1141 as subcategory_id
,'Avg. Target Frequency' as Subcategory_Name
,11113 as Metric_Id
,'Avg. Target Frequency' as Metric_Name
,max(avg_tgt_ind_freq) as Metric_Val1

,case when (UPPER(trim(grouped_indication))='ALLIANCE' and UPPER(trim(indication)) = 'ALLIANCE')
	or (UPPER(trim(grouped_indication))='ALL' and UPPER(trim(indication)) = 'ALL')
	then 0 
	else 1 end as Metric_Val2
,max(hcp_tgt_ind_cnt) as metric_val3
from eisai_prod_db.idl_call_activity
where target_flag = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,indication

UNION ALL

-- Details by Indication for Details Attainment
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
11 as Panel_Id
,'Details by Indication'  as Panel_Name
,cast(concat('11',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as category_id
,indication as category_name
,1151 as subcategory_id
,'Details Attainment' as Subcategory_Name
,11114 as Metric_Id
,'Details Attainment' as Metric_Name
,sum(call_details) as Metric_Val1

,case when (UPPER(trim(grouped_indication))='ALLIANCE' and UPPER(trim(indication)) = 'ALLIANCE')
	or (UPPER(trim(grouped_indication))='ALL' and UPPER(trim(indication)) = 'ALL')
	then 0 
	else 1 end as Metric_Val2
,max(call_goal.call_goals_prorated) as metric_val3
from
(
select idl_call.* , goals.call_goals_prorated from eisai_prod_db.idl_call_activity as idl_call 
left join (select distinct organization, grouped_indication, src_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, call_goals_prorated from eisai_prod_db.f_call_goals) as goals
on  upper(trim(idl_call.org))    = upper(trim(goals.organization))
and idl_call.time_bckt_nm        = goals.time_bckt_nm
and idl_call.time_bckt_cd        = goals.time_bckt_cd
and idl_call.time_bckt_strt      = goals.time_bckt_strt
and idl_call.time_bckt_end       = goals.time_bckt_end
and upper(trim(idl_call.grouped_indication))  = upper(trim(goals.grouped_indication))
and upper(trim(idl_call.indication))			 = upper(trim(goals.src_indication))
) as call_goal
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,indication

UNION ALL


-- Details by Channel for Product Position Details

select call_goal.org, call_goal.brand_name, call_goal.grouped_indication, call_goal.time_bckt_nm, call_goal.time_bckt_cd, call_goal.time_bckt_strt, call_goal.time_bckt_end,
22 as Panel_Id
,'Details by Channel'  as Panel_Name
,cast(concat('22',cast(dense_rank() over (partition by null order by mdm_call_channel) as varchar) )as int) as category_id
,mdm_call_channel as category_name
,cast(concat('222',cast(dense_rank() over (partition by null order by product_position) as varchar) )as int) as subcategory_id
,product_position as Subcategory_Name
,22221 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,max(call_goal.call_goals_prorated) as Metric_Val2
,max(target_count) as metric_val3
from 
(
select idl_call.* , goals.call_goals_prorated from eisai_prod_db.idl_call_activity as idl_call 
left join (select distinct organization, grouped_indication, src_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, call_goals_prorated from eisai_prod_db.f_call_goals) as goals
on  upper(trim(idl_call.org))    = upper(trim(goals.organization))
and idl_call.time_bckt_nm        = goals.time_bckt_nm
and idl_call.time_bckt_cd        = goals.time_bckt_cd
and idl_call.time_bckt_strt      = goals.time_bckt_strt
and idl_call.time_bckt_end       = goals.time_bckt_end
and upper(trim(idl_call.grouped_indication))  = upper(trim(goals.grouped_indication))
and upper(trim(idl_call.indication))			 = upper(trim(goals.src_indication))
) as call_goal
where indication = grouped_indication
group by call_goal.org, call_goal.brand_name, call_goal.grouped_indication, call_goal.time_bckt_nm, call_goal.time_bckt_cd, call_goal.time_bckt_strt, call_goal.time_bckt_end,product_position,mdm_call_channel

UNION ALL 

--Target Reach Ratio by Channel
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
22 as Panel_Id
,'Details by Channel'  as Panel_Name
,cast(concat('22',cast(dense_rank() over (partition by null order by mdm_call_channel) as varchar) )as int) as category_id
,mdm_call_channel as category_name
,2231 as subcategory_id
,'Target Reach Channel Percent' as Subcategory_Name
,22222 as Metric_Id
,'Target Reach Ratio By Channel' as Metric_Name
,max(target_reach_ratio_channel) as Metric_Val1
,0 as Metric_Val2
,max(target_count) as metric_val3
from eisai_prod_db.idl_call_activity
where target_flag = 'Y' and indication = grouped_indication
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,mdm_call_channel

UNION ALL

-- Details by Target for Product Position Details
select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
33 as Panel_Id
,'Details by Target'  as Panel_Name
,cast(concat('33',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id

,target_type as category_name
,cast(concat('333',cast(dense_rank() over (partition by null order by product_position) as varchar) )as int) as subcategory_id
,product_position as Subcategory_Name
,33331 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_count) as metric_val3
from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,product_position,target_type

) fnl
where category_name <>'NA'

UNION ALL 

-- Details by Target for Call Channels Details
select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
33 as Panel_Id
,'Details by Target'  as Panel_Name
,cast(concat('33',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id


,target_type as category_name
,cast(concat('334',cast(dense_rank() over (partition by null order by mdm_call_channel) as varchar) )as int) as subcategory_id
,mdm_call_channel as Subcategory_Name
,33331 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_count) as metric_val3
from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,mdm_call_channel,target_type

) fnl
where category_name <>'NA'

UNION ALL

-- Details by Target for Target Reach Details 

select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
33 as Panel_Id
,'Details by Target'  as Panel_Name
,cast(concat('33',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id

,target_type as category_name
,3351 as subcategory_id
,'Target Reach By Target Percent' as Subcategory_Name
,33332 as Metric_Id
,'Target Reach Ratio By Targets' as Metric_Name
,max(target_reach_ratio_by_target) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_count) as Metric_Val3

from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication and target_flag = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,target_type

) fnl
where category_name <>'NA'

UNION ALL

-- Target Reach (TILES)

select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
55 as Panel_Id
,'Aggregated'  as Panel_Name
,551 as category_id

,'Target Reach' as category_name
,5551 as subcategory_id
,'Target Reach Tiles Percent' as Subcategory_Name
,55551 as Metric_Id
,'Target Reach Ratio' as Metric_Name
,max(target_reach_ratio) as Metric_Val1
,0 as Metric_Val2
,max(target_count) as Metric_Val3
from eisai_prod_db.idl_call_activity
where target_flag = 'Y' and indication = grouped_indication
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end
 
) fnl
where category_name <>'NA'

UNION ALL

-- Frequency (TILES)
select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
44 as Panel_Id
,'Aggregated'  as Panel_Name
,441 as category_id
,'Frequency' as category_name
,4441 as subcategory_id
,'Avg. Target Frequency'as Subcategory_Name
,44441 as Metric_Id
,'Avg. Target Frequency' as Metric_Name
,max(avg_tgt_freq) as Metric_Val1
,0 as Metric_Val2
,max(target_reach) as Metric_Val3
from eisai_prod_db.idl_call_activity
where target_flag = 'Y' and indication = grouped_indication
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end

UNION ALL

-- Details by Early-Adopter for Product Position Details
select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
66 as Panel_Id
,'Details by Early Adopter'  as Panel_Name
,cast(concat('66',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id

,target_type as category_name
,cast(concat('666',cast(dense_rank() over (partition by null order by product_position) as varchar) )as int) as subcategory_id
,product_position as Subcategory_Name
,66661 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_ea_count) as Metric_Val3
from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication and early_adopter = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,product_position,target_type

) fnl
where category_name <>'NA'

UNION ALL 

-- Details by Early-Adopter for Call Channels Details
select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
66 as Panel_Id
,'Details by Early Adopter'  as Panel_Name
,cast(concat('66',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id

,target_type as category_name
,cast(concat('667',cast(dense_rank() over (partition by null order by mdm_call_channel) as varchar) )as int) as subcategory_id
,mdm_call_channel as Subcategory_Name
,66661 as Metric_Id
,'Details' as Metric_Name
,sum(call_details) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_ea_count) as Metric_Val3
from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication and early_adopter = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,mdm_call_channel,target_type

) fnl
where category_name <>'NA'

UNION ALL

-- Details by Early-Adopter for Target Reach Details 

select * from (select
org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
66 as Panel_Id
,'Details by Early Adopter'  as Panel_Name
,cast(concat('66',cast(dense_rank() over (partition by null order by (
target_type)) as varchar) )as int) as category_id

,target_type as category_name
,6681 as subcategory_id
,'Target Reach By Early Adopter Percent' as Subcategory_Name
,66662 as Metric_Id
,'Target Reach Ratio By Early Adopter' as Metric_Name
,max(target_reach_ratio_by_ea) as Metric_Val1
,0 as Metric_Val2
,max(target_seg_ea_count) as Metric_Val3
from (select * , case when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'A' then 'EISAI A'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'B' then 'EISAI B'
	when upper(TRIM(org))='EISAI' and target_flag='Y' and UPPER(trim(segment)) = 'C' then 'EISAI C'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'HIGH' then 'MERCK HIGH'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'MEDIUM' then 'MERCK MEDIUM'
	when upper(TRIM(org))='MERCK' and target_flag='Y' and UPPER(trim(segment)) = 'LOW' then 'MERCK LOW'
	when upper(TRIM(org))='ALLIANCE' and target_flag='Y' then 'ALLIANCE TARGETS'
else 'NA' end as target_type from eisai_prod_db.idl_call_activity) as atvy

where indication = grouped_indication and early_adopter = 'Y'
group by org, brand_name, grouped_indication,  time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,target_type

) fnl
where category_name <>'NA'
);