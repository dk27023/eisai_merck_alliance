drop table if exists eisai_prod_db.stg_call_txn_merck;
create table eisai_prod_db.stg_call_txn_merck as
select
DATE as split_week_end_date
,'NULL' as prod_id
,'NULL' as prod_name
,'NULL' as brand_id
,'Lenvima' as brand_name
,case when UPPER(trim(Split_part(MSTR_PROD_NM, '-', 2))) = 'ENDO' then 'EC'
else UPPER(trim(Split_part(MSTR_PROD_NM, '-', 2))) end as indication
,CONCAT('P', POSITION) as product_position
,IMS_ALTRNT_ID as hcp_id
,NPI_ALTRNT_ID as npi
,CALLS as total_calls
,Eligible_Call as call_status
,call_topic_desc as call_channel
from eisai_prod_db.ldg_call_txn_merck;