drop table if exists eisai_prod_db.ldg_call_targets_merck;
create table eisai_prod_db.ldg_call_targets_merck
(
Party_Id varchar(255)
,IMS_ID varchar(255)
,NPI_ID varchar(255)
,Indication varchar(255)
,Segment varchar(255)
,Early_Adopter varchar(255)
);

truncate table eisai_prod_db.ldg_call_targets_merck ;

copy eisai_prod_db.ldg_call_targets_merck 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Merck_Targets/S1_2022/Merck_Targets.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT as CSV
IGNOREHEADER 1
;

select * from eisai_prod_db.ldg_call_targets_merck ;