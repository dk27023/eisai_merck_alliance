drop table if exists eisai_prod_db.f_int_call_activity;
create table eisai_prod_db.f_int_call_activity as 
(
--EISAI

select f_call_atvy.org, f_call_atvy.split_week_end, f_call_atvy.prod_id, f_call_atvy.prod_name, f_call_atvy.brand_id, f_call_atvy.brand_name, f_call_atvy.indication,f_call_atvy.product_position, f_call_atvy.hcp_id, 'NULL' as npi, f_call_atvy.total_calls, f_call_atvy.call_status, f_call_atvy.call_channel, f_call_atvy.recency, f_call_atvy.time_bckt_strt, f_call_atvy.time_bckt_end, f_call_atvy.time_bckt_nm, f_call_atvy.time_bckt_cd, f_call_atvy.time_bckt_freq, f_call_atvy.time_bckt_indication, 
case when CALL_TRGT.target_hcp_id is not null then 'Y' else 'N' end as TARGET_FLAG
,COALESCE(CALL_TRGT.early_adopter, 'N') as early_adopter, COALESCE(segment, 'NA') as segment,
channel_ref.CALL_CHANNEL as MDM_Call_Channel
from
(
select 
'EISAI' as ORG, CALL_KPI_REF.*, TIME_BCKT.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_call_kpi_eisai where call_status = 'Y') CALL_KPI_REF 
left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where UPPER(trim(source)) = 'ACTIVITY') TIME_BCKT
on 
CALL_KPI_REF.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end
where TIME_BCKT.time_bckt_cd in ('R4W', 'R13W', 'R12M', 'WTD', 'MTD', 'QTD1 (W)', 'FYTD1 (W)', 'W1' ,'W2' ,'W3' ,'W4' ,'W5' ,'W6' ,'W7' ,'W8' ,'W9' ,'W10' ,'W11' ,'W12' ,'W13' ,'W14' ,'W15' ,'W16' ,'W17' ,'W18' ,'W19' ,'W20' ,'W21' ,'W22' ,'W23' ,'W24' ,'W25' ,'W26', 'M1' ,'M2' ,'M3' ,'M4' ,'M5' ,'M6' ,'M7' ,'M8' ,'M9' ,'M10' ,'M11' ,'M12' ,'M13' ,'M14' ,'M15' ,'M16' ,'M17' ,'M18' ,'M19' ,'M20' ,'M21' ,'M22' ,'M23' ,'M24')

union all

select 
'EISAI' as ORG, CALL_KPI_REF.*, TIME_BCKT.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_call_kpi_eisai where call_status = 'Y') CALL_KPI_REF 
left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where UPPER(trim(source)) = 'ACTIVITY') TIME_BCKT
on  
CALL_KPI_REF.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end and CALL_KPI_REF.indication = TIME_BCKT.time_bckt_indication
where TIME_BCKT.time_bckt_cd = 'LTD (W)'
) as f_call_atvy

left join (select distinct ims_id as target_hcp_id, indication as target_indication, early_adopter, upper(trim(segment)) as segment, npi_id from eisai_prod_db.stg_call_targets where UPPER(trim(ORG_TARGET)) = 'EISAI') CALL_TRGT
on f_call_atvy.hcp_id = CALL_TRGT.target_hcp_id and UPPER(trim(f_call_atvy.indication)) = upper(trim(CALL_TRGT.target_indication))

left join (select distinct EISAI_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(f_call_atvy.call_channel)) = upper(trim(channel_ref.eisai_call_channel))


UNION ALL

--MERCK

select f_call_atvy.org, f_call_atvy.split_week_end, f_call_atvy.prod_id, f_call_atvy.prod_name, f_call_atvy.brand_id, f_call_atvy.brand_name, f_call_atvy.indication,f_call_atvy.product_position, f_call_atvy.hcp_id, f_call_atvy.npi, f_call_atvy.total_calls, f_call_atvy.call_status, f_call_atvy.call_channel, f_call_atvy.recency, f_call_atvy.time_bckt_strt, f_call_atvy.time_bckt_end, f_call_atvy.time_bckt_nm, f_call_atvy.time_bckt_cd, f_call_atvy.time_bckt_freq, f_call_atvy.time_bckt_indication, 
case when CALL_TRGT.target_hcp_id is not null then 'Y' else 'N' end as TARGET_FLAG
,COALESCE(CALL_TRGT.early_adopter, 'N') as early_adopter, COALESCE(segment, 'NA') as segment,
channel_ref.CALL_CHANNEL as MDM_Call_Channel
from
(
select 
'MERCK' as ORG, CALL_TXN_MERCK.*, TIME_BCKT.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.stg_call_txn_merck where call_status = 'Y') CALL_TXN_MERCK 
left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where UPPER(trim(source)) = 'ACTIVITY') TIME_BCKT
on 
CALL_TXN_MERCK.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end
where TIME_BCKT.time_bckt_cd in ('R4W', 'R13W', 'R12M', 'WTD', 'MTD', 'QTD1 (W)', 'FYTD1 (W)', 'W1' ,'W2' ,'W3' ,'W4' ,'W5' ,'W6' ,'W7' ,'W8' ,'W9' ,'W10' ,'W11' ,'W12' ,'W13' ,'W14' ,'W15' ,'W16' ,'W17' ,'W18' ,'W19' ,'W20' ,'W21' ,'W22' ,'W23' ,'W24' ,'W25' ,'W26', 'M1' ,'M2' ,'M3' ,'M4' ,'M5' ,'M6' ,'M7' ,'M8' ,'M9' ,'M10' ,'M11' ,'M12' ,'M13' ,'M14' ,'M15' ,'M16' ,'M17' ,'M18' ,'M19' ,'M20' ,'M21' ,'M22' ,'M23' ,'M24')

union all

select 
'MERCK' as ORG, CALL_TXN_MERCK.*, TIME_BCKT.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.stg_call_txn_merck where call_status = 'Y') CALL_TXN_MERCK 
left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where UPPER(trim(source)) = 'ACTIVITY') TIME_BCKT
on  
CALL_TXN_MERCK.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end and CALL_TXN_MERCK.indication = TIME_BCKT.time_bckt_indication
where TIME_BCKT.time_bckt_cd = 'LTD (W)'
) as f_call_atvy

left join (select distinct ims_id as target_hcp_id, indication as target_indication, early_adopter, upper(trim(segment)) as segment, npi_id from eisai_prod_db.stg_call_targets where UPPER(trim(ORG_TARGET)) = 'MERCK') CALL_TRGT
on (f_call_atvy.hcp_id = CALL_TRGT.target_hcp_id --or f_call_atvy.npi = CALL_TRGT.npi_id -- AA- To be revisited
) and upper(trim(f_call_atvy.indication)) = upper(trim(CALL_TRGT.target_indication))

left join (select distinct MERCK_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(f_call_atvy.call_channel)) = upper(trim(channel_ref.merck_call_channel))


UNION ALL

--ALLIANCE

select f_call_atvy.org, f_call_atvy.split_week_end, f_call_atvy.prod_id, f_call_atvy.prod_name, f_call_atvy.brand_id, f_call_atvy.brand_name, f_call_atvy.indication,f_call_atvy.product_position, f_call_atvy.hcp_id, f_call_atvy.npi, f_call_atvy.total_calls, f_call_atvy.call_status, f_call_atvy.call_channel, f_call_atvy.recency, f_call_atvy.time_bckt_strt, f_call_atvy.time_bckt_end, f_call_atvy.time_bckt_nm, f_call_atvy.time_bckt_cd, f_call_atvy.time_bckt_freq, f_call_atvy.time_bckt_indication, 
case when CALL_TRGT.target_hcp_id is not null then 'Y' else 'N' end as TARGET_FLAG
,COALESCE(CALL_TRGT.early_adopter, 'N') as early_adopter, COALESCE(segment, 'NA') as segment,
f_call_atvy.mdm_call_channel
from
(
select 
'ALLIANCE' as ORG, CALL_TXN_ALL.*, TIME_BCKT.*
from
(
select merck_calls.split_week_end, merck_calls.prod_id, merck_calls.prod_name, merck_calls.brand_id, merck_calls.brand_name, merck_calls.indication,merck_calls.product_position, merck_calls.hcp_id, merck_calls.npi, merck_calls.total_calls, merck_calls.call_status, merck_calls.call_channel, channel_ref.call_channel as mdm_call_channel from 
(
select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.stg_call_txn_merck where call_status = 'Y'
) merck_calls
left join (select distinct MERCK_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(merck_calls.call_channel)) = upper(trim(channel_ref.merck_call_channel))

UNION ALL 

select eisai_calls.split_week_end, eisai_calls.prod_id, eisai_calls.prod_name, eisai_calls.brand_id, eisai_calls.brand_name, eisai_calls.indication,eisai_calls.product_position, eisai_calls.hcp_id, 'NULL' as npi, eisai_calls.total_calls, eisai_calls.call_status, eisai_calls.call_channel,channel_ref.call_channel as mdm_call_channel from
(
select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_call_kpi_eisai where call_status = 'Y'
) eisai_calls
left join (select distinct EISAI_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(eisai_calls.call_channel)) = upper(trim(channel_ref.eisai_call_channel))
) CALL_TXN_ALL 
left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where upper(trim(source)) = 'ACTIVITY') TIME_BCKT
on 
CALL_TXN_ALL.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end
where TIME_BCKT.time_bckt_cd in ('R4W', 'R13W', 'R12M', 'WTD', 'MTD', 'QTD1 (W)', 'FYTD1 (W)', 'W1' ,'W2' ,'W3' ,'W4' ,'W5' ,'W6' ,'W7' ,'W8' ,'W9' ,'W10' ,'W11' ,'W12' ,'W13' ,'W14' ,'W15' ,'W16' ,'W17' ,'W18' ,'W19' ,'W20' ,'W21' ,'W22' ,'W23' ,'W24' ,'W25' ,'W26', 'M1' ,'M2' ,'M3' ,'M4' ,'M5' ,'M6' ,'M7' ,'M8' ,'M9' ,'M10' ,'M11' ,'M12' ,'M13' ,'M14' ,'M15' ,'M16' ,'M17' ,'M18' ,'M19' ,'M20' ,'M21' ,'M22' ,'M23' ,'M24')


union all

select 
'ALLIANCE' as ORG, CALL_TXN_ALL.*, TIME_BCKT.*
from (
select merck_calls.split_week_end, merck_calls.prod_id, merck_calls.prod_name, merck_calls.brand_id, merck_calls.brand_name, merck_calls.indication,merck_calls.product_position, merck_calls.hcp_id, merck_calls.npi, merck_calls.total_calls, merck_calls.call_status, merck_calls.call_channel,channel_ref.call_channel as mdm_call_channel from 
(
select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.stg_call_txn_merck where call_status = 'Y'
) merck_calls
left join (select distinct MERCK_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(merck_calls.call_channel)) = upper(trim(channel_ref.merck_call_channel))

UNION ALL 

select eisai_calls.split_week_end, eisai_calls.prod_id, eisai_calls.prod_name, eisai_calls.brand_id, eisai_calls.brand_name, eisai_calls.indication,eisai_calls.product_position, eisai_calls.hcp_id, 'NULL' as npi, eisai_calls.total_calls, eisai_calls.call_status, eisai_calls.call_channel,channel_ref.call_channel as mdm_call_channel from
(
select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_call_kpi_eisai where call_status = 'Y'
) eisai_calls
left join (select distinct EISAI_CALL_CHANNEL, call_channel from eisai_prod_db.m_chnl_xref) channel_ref
on upper(trim(eisai_calls.call_channel)) = upper(trim(channel_ref.eisai_call_channel))
) CALL_TXN_ALL

left join (select distinct recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where UPPER(trim(source)) = 'ACTIVITY') TIME_BCKT
on  
CALL_TXN_ALL.split_week_end between TIME_BCKT.time_bckt_strt and TIME_BCKT.time_bckt_end and CALL_TXN_ALL.indication = TIME_BCKT.time_bckt_indication
where TIME_BCKT.time_bckt_cd = 'LTD (W)'
) as f_call_atvy

left join (select distinct ims_id as target_hcp_id, indication as target_indication, early_adopter, upper(trim(segment)) as segment, npi_id from eisai_prod_db.stg_call_targets where UPPER(trim(ORG_TARGET)) = 'ALLIANCE') CALL_TRGT
on f_call_atvy.hcp_id = CALL_TRGT.target_hcp_id and upper(trim(f_call_atvy.indication)) = upper(trim(CALL_TRGT.target_indication))
);


drop table if exists eisai_prod_db.f_call_activity_int1;
create table eisai_prod_db.f_call_activity_int1 as
select f_call_atvy.*, 
case when D.target_count is not null then target_count else 0 end as TARGET_COUNT
from 
(
select org, prod_id, prod_name, brand_id, brand_name, i_xref.grouped_indication, call_ind.indication, product_position
, hcp_id,  call_status, recency, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, target_flag
, early_adopter, segment,mdm_call_channel
, sum(total_calls) as total_calls

from 
(
select org, prod_id, prod_name, brand_id, brand_name, xref.grouped_indication as indication, product_position, hcp_id, total_calls, call_status, recency, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, target_flag, early_adopter, segment,mdm_call_channel
from eisai_prod_db.f_int_call_activity as calls
left join eisai_prod_db.m_indication_grouping as xref
on upper(trim(xref.indication))=upper(trim(calls.indication))
) as call_ind
left join eisai_prod_db.m_indication_grouping as i_xref
on upper(trim(i_xref.indication))=upper(trim(call_ind.indication))

group by  org, prod_id, prod_name, brand_id, brand_name, i_xref.grouped_indication, call_ind.indication, product_position
, hcp_id,  call_status, recency, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, target_flag
, early_adopter, segment,mdm_call_channel
)  as f_call_atvy

left join 
(
select upper(org_target) as org_target,indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' group by 1,2
union
select upper(org_target) as org_target,'Alliance' as indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' and upper(indication) in ('EC','RCC')  group by 1,2
union
select upper(org_target) as org_target,'All' as indication, count(distinct ims_id) as target_count  from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' group by 1,2

UNION
select upper(org_target) as org_target,indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' group by 1,2
UNION
select upper(org_target) as org_target, 'Alliance' as indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' and upper(indication) in ('EC','RCC') group by 1,2
UNION
select upper(org_target) as org_target, 'All' as indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' group by 1,2

UNION
select org_target,indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' group by 1,2
UNION
select org_target, 'Alliance' as indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' and upper(indication) in ('EC','RCC') group by 1,2
UNION
select org_target, 'All' as indication, count(distinct ims_id) as target_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' group by 1,2
) D 
on upper(trim(f_call_atvy.indication)) = upper(trim(D.indication))
and upper(trim(f_call_atvy.org)) = upper(trim(D.org_target));


drop table if exists eisai_prod_db.f_call_activity_int2;
create table eisai_prod_db.f_call_activity_int2 as
select f_int1.*,
case when D.target_seg_count is not null then target_seg_count else 0 end as target_seg_count
 

from eisai_prod_db.f_call_activity_int1 as f_int1

left join 
(
select upper(org_target) as org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' group by 1,2,3
union
select upper(org_target) as org_target,'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' and upper(indication) in ('EC','RCC')  group by 1,2,3
union
select upper(org_target) as org_target,'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count  from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' group by 1,2,3

UNION
select upper(org_target) as org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' group by 1,2,3
UNION
select upper(org_target) as org_target, 'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' and upper(indication) in ('EC','RCC') group by 1,2,3
UNION
select upper(org_target) as org_target, 'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' group by 1,2,3

UNION
select org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' group by 1,2,3
UNION
select org_target, 'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' and upper(indication) in ('EC','RCC') group by 1,2,3
UNION
select org_target, 'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' group by 1,2,3
) D 
on upper(trim(f_int1.indication)) = upper(trim(D.indication))
and upper(trim(f_int1.org)) = upper(trim(D.org_target))
and upper(trim(f_int1.segment)) = upper(trim(D.segment));



drop table if exists eisai_prod_db.f_call_activity;
create table eisai_prod_db.f_call_activity as
select f_int2.*, case when D.target_seg_ea_count is not null then target_seg_ea_count else 0 end as target_seg_ea_count

from eisai_prod_db.f_call_activity_int2 as f_int2

left join 
(
select upper(org_target) as org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI'  and early_adopter='Y' group by 1,2,3
union
select upper(org_target) as org_target,'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI' and upper(indication) in ('EC','RCC') and early_adopter='Y' group by 1,2,3
union
select upper(org_target) as org_target,'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count  from eisai_prod_db.stg_call_targets where upper(trim(org_target))='EISAI'and early_adopter='Y' group by 1,2,3

UNION
select upper(org_target) as org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK'and early_adopter='Y' group by 1,2,3
UNION
select upper(org_target) as org_target, 'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK' and upper(indication) in ('EC','RCC')and early_adopter='Y' group by 1,2,3
UNION
select upper(org_target) as org_target, 'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='MERCK'and early_adopter='Y' group by 1,2,3

UNION
select org_target,indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE'and early_adopter='Y' group by 1,2,3
UNION
select org_target, 'Alliance' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE' and upper(indication) in ('EC','RCC')and early_adopter='Y' group by 1,2,3
UNION
select org_target, 'All' as indication, upper(trim(segment)) as segment, count(distinct ims_id) as target_seg_ea_count from eisai_prod_db.stg_call_targets where upper(trim(org_target))='ALLIANCE'and early_adopter='Y' group by 1,2,3
) D 
on upper(trim(f_int2.indication)) = upper(trim(D.indication))
and upper(trim(f_int2.org)) = upper(trim(D.org_target))
and upper(trim(f_int2.segment)) = upper(trim(D.segment));