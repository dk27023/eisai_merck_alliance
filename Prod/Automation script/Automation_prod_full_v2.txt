import os
import sys
import subprocess, traceback
import json
import logging
import datetime, re

!pip install boto3

import boto3

session = boto3.session.Session(profile_name='EISAI_BUCKET')
s3 = session.resource('s3')
bucket = s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01')

!pip install psycopg2-binary

import psycopg2
import pandas as pd
import numpy as np

db_name='redshift_db_awb'
host_name='aws-a0077-use1-1a-p-red-shrd-awb-shrd-prod-793.cwnkfmpsr9xw.us-east-1.redshift.amazonaws.com'
port_number=5439
user_name='awb_redshift_user'
user_password='Lq9XGNRvzV'
con_rs=psycopg2.connect(dbname= db_name, host=host_name, port= port_number, user= user_name, password= user_password)

cursor_rs = con_rs.cursor()

clean_up = "aws s3 rm s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Execution/Eisai_txn --recursive"
print(clean_up)
os.popen(clean_up).readlines()


latest = "aws s3 ls s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Eisai/calls_kpi | tail -n 1 | awk '{print $4}'"
latest_file =os.popen(latest).readlines()
print(latest_file)


for i in latest_file:
    print ("Transferring file "+str(i).strip()+" to the Latest/Execution/Eisai_txn/")
    cp_file = "aws s3 cp s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Eisai/"+str(i).strip()+" s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Execution/Eisai_txn/eisai_call_kpi.txt"
    os.popen(cp_file).readlines()
        
print("EISAI DONE")


clean_up = "aws s3 rm s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Execution/Merck_txn --recursive"
print(clean_up)
os.popen(clean_up).readlines()
    

latest = !aws s3 ls s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Merck/ | awk '{for(i=4;i<=NF;i++){printf "%s ", $i}; printf "\n"}' | tail -n 1
print(latest)

for i in latest:
    print ("Transferring file "+str(i).strip()+" to the Latest/Execution/Merck_txn/")
    cp_file = "aws s3 cp s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Execution_Files/Merck/"+"\""+str(i).strip()+"\""+" s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Execution/Merck_txn/merck_call_kpi.txt"
    os.popen(cp_file).readlines()
    
print("MERCK DONE")



clean_up = "aws s3 rm s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales/Eisai_sales_kpi --recursive"
print(clean_up)
os.popen(clean_up).readlines()


latest = "aws s3 ls s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Sales_Files/Eisai/sales_kpi/sales_kpi | tail -n 1 | awk '{print $4}'"
latest_file =os.popen(latest).readlines()
print(latest_file)


for i in latest_file:
    print ("Transferring file "+str(i).strip()+" to the Latest/Sales/Eisai_sales_kpi/")
    cp_file = "aws s3 cp s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Sales_Files/Eisai/sales_kpi/"+str(i).strip()+" s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales/Eisai_sales_kpi/eisai_sales_kpi.txt"
    os.popen(cp_file).readlines()
        
print("EISAI SALES KPI DONE")


clean_up = "aws s3 rm s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales/Eisai_sales_writer --recursive"
print(clean_up)
os.popen(clean_up).readlines()


latest = "aws s3 ls s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Sales_Files/Eisai/sales_writer/sales_writer | tail -n 1 | awk '{print $4}'"
latest_file =os.popen(latest).readlines()
print(latest_file)


for i in latest_file:
    print ("Transferring file "+str(i).strip()+" to the Latest/Sales/Eisai_sales_writer/")
    cp_file = "aws s3 cp s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Sales_Files/Eisai/sales_writer/"+str(i).strip()+" s3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales/Eisai_sales_writer/eisai_sales_writer.txt"
    os.popen(cp_file).readlines()
        
print("EISAI SALES WRITER DONE")


cursor_rs = con_rs.cursor()

s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/ldg_call_kpi_eisai.sql', '/home/dk27023/EISAI/Prod/Execution/ldg_call_kpi_eisai.txt')

eisai = open('/home/dk27023/EISAI/Prod/Execution/ldg_call_kpi_eisai.txt')
eisai_txn = eisai.read()

cursor_rs.execute(eisai_txn)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/ldg_call_txn_merck.sql', '/home/dk27023/EISAI/Prod/Execution/ldg_call_txn_merck.txt')

merck = open('/home/dk27023/EISAI/Prod/Execution/ldg_call_txn_merck.txt')
merck_txn = merck.read()

cursor_rs.execute(merck_txn)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/stg_call_txn_merck.sql', '/home/dk27023/EISAI/Prod/Execution/stg_call_txn_merck.txt')

merck_stg = open('/home/dk27023/EISAI/Prod/Execution/stg_call_txn_merck.txt')
merck_txn_stg = merck_stg.read()

cursor_rs.execute(merck_txn_stg)



s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/ldg_sales_kpi.sql', '/home/dk27023/EISAI/Prod/Sales/ldg_sales_kpi.txt')

sales_kpi = open('/home/dk27023/EISAI/Prod/Sales/ldg_sales_kpi.txt')
eisai_sales = sales_kpi.read()

cursor_rs.execute(eisai_sales)



s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/other/ctl_job_run.sql', '/home/dk27023/EISAI/Prod/Other/ctl_job_run.txt')

ctl_job = open('/home/dk27023/EISAI/Prod/Other/ctl_job_run.txt')
ctl_job_run = ctl_job.read()

cursor_rs.execute(ctl_job_run)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/d_time/flg.sql', '/home/dk27023/EISAI/Prod/d_time/flg.txt')

flg = open('/home/dk27023/EISAI/Prod/d_time/flg.txt')
d_time_flg = flg.read()

cursor_rs.execute(d_time_flg)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/d_time/d_time_bckt_Sales.sql', '/home/dk27023/EISAI/Prod/d_time/d_time_bckt_Sales.txt')

sales_d_time = open('/home/dk27023/EISAI/Prod/d_time/d_time_bckt_Sales.txt')
sales_d_time_bckt = sales_d_time.read()

cursor_rs.execute(sales_d_time_bckt)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/d_time/d_time_bckt_Acitivity.sql', '/home/dk27023/EISAI/Prod/d_time/d_time_bckt_Acitivity.txt')

activity_d_time = open('/home/dk27023/EISAI/Prod/d_time/d_time_bckt_Acitivity.txt')
activity_d_time_bckt = activity_d_time.read()

cursor_rs.execute(activity_d_time_bckt)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/d_time/d_time_bckt.sql', '/home/dk27023/EISAI/Prod/d_time/d_time_bckt.txt')

d_time = open('/home/dk27023/EISAI/Prod/d_time/d_time_bckt.txt')
d_time_bckt = d_time.read()

cursor_rs.execute(d_time_bckt)

con_rs.commit()


cursor_rs = con_rs.cursor()

s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/f_call_activity.sql', '/home/dk27023/EISAI/Prod/Execution/f_call_activity.txt')

fact = open('/home/dk27023/EISAI/Prod/Execution/f_call_activity.txt')
fact_layer = fact.read()

cursor_rs.execute(fact_layer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/idl_call_activity.sql', '/home/dk27023/EISAI/Prod/Execution/idl_call_activity.txt')

idl = open('/home/dk27023/EISAI/Prod/Execution/idl_call_activity.txt')
idl_layer = idl.read()

cursor_rs.execute(idl_layer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/f_call_goals.sql', '/home/dk27023/EISAI/Prod/Execution/f_call_goals.txt')

call_goals = open('/home/dk27023/EISAI/Prod/Execution/f_call_goals.txt')
call_goals_layer = call_goals.read()

cursor_rs.execute(call_goals_layer)

s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Execution/int_call_activity.sql', '/home/dk27023/EISAI/Prod/Execution/int_call_activity.txt')

int_exe = open('/home/dk27023/EISAI/Prod/Execution/int_call_activity.txt')
int_layer = int_exe.read()

cursor_rs.execute(int_layer)

con_rs.commit()


cursor_rs = con_rs.cursor()

s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/ldg_sales_writer.sql', '/home/dk27023/EISAI/Prod/Sales/ldg_sales_writer.txt')

sales_writer = open('/home/dk27023/EISAI/Prod/Sales/ldg_sales_writer.txt')
eisai_sales_writer = sales_writer.read()

cursor_rs.execute(eisai_sales_writer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/f_sales_kpi.sql', '/home/dk27023/EISAI/Prod/Sales/f_sales_kpi.txt')

sales_fact = open('/home/dk27023/EISAI/Prod/Sales/f_sales_kpi.txt')
sales_fact_layer = sales_fact.read()

cursor_rs.execute(sales_fact_layer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/idl_sales_kpi.sql', '/home/dk27023/EISAI/Prod/Sales/idl_sales_kpi.txt')

idl_sales_kpi = open('/home/dk27023/EISAI/Prod/Sales/idl_sales_kpi.txt')
idl_sales_kpi_layer = idl_sales_kpi.read()

cursor_rs.execute(idl_sales_kpi_layer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/idl_sales_writers.sql', '/home/dk27023/EISAI/Prod/Sales/idl_sales_writers.txt')

idl_sales_writer = open('/home/dk27023/EISAI/Prod/Sales/idl_sales_writers.txt')
idl_sales_writer_layer = idl_sales_writer.read()

cursor_rs.execute(idl_sales_writer_layer)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/Sales/int_sales_writer.sql', '/home/dk27023/EISAI/Prod/Sales/int_sales_writer.txt')

int_sales_writer = open('/home/dk27023/EISAI/Prod/Sales/int_sales_writer.txt')
int_sales_writer_layer = int_sales_writer.read()

cursor_rs.execute(int_sales_writer_layer)

con_rs.commit()


cursor_rs = con_rs.cursor()

s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/other/i_button_time_bckt.sql', '/home/dk27023/EISAI/Prod/Other/i_button_time_bckt.txt')

i_button = open('/home/dk27023/EISAI/Prod/Other/i_button_time_bckt.txt')
i_button_time_bckt = i_button.read()

cursor_rs.execute(i_button_time_bckt)


s3.Bucket('aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01').download_file('Prod/Jupyter/Codes/other/refresh_date_tbl.sql', '/home/dk27023/EISAI/Prod/Other/refresh_date_tbl.txt')

refresh = open('/home/dk27023/EISAI/Prod/Other/refresh_date_tbl.txt')
refresh_date = refresh.read()

cursor_rs.execute(refresh_date)

con_rs.commit()



df = pd.read_sql(""" select var_val from eisai_prod_db.ctl_job_run""",con_rs)
a = df.loc[0]['var_val']
b = str(a).strip()
b = b.replace("-", "")
print(b)


execution_bkp = """create table eisai_prod_db.int_call_activity_bkp_""" + str(b).strip() + """ as select * from eisai_prod_db.int_call_activity;"""
print(execution_bkp)

cursor_rs.execute(execution_bkp)
con_rs.commit()

sales_bkp = """create table eisai_prod_db.int_sales_writer_bkp_""" + str(b).strip() + """ as select * from eisai_prod_db.int_sales_writer;"""
print(sales_bkp)

cursor_rs.execute(sales_bkp)
con_rs.commit()

cursor_rs.close()
con_rs.close()
