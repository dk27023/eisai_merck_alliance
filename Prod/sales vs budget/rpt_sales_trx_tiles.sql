drop table if exists eisai_prod_db.rpt_sales_trx_tiles;
create table eisai_prod_db.rpt_sales_trx_tiles AS

(
select therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'TRX_BUDGET' as Metric_Name, trx_budget as Metric_Value1, '0' as Metric_Value2
from eisai_prod_db.stg_trx_budget_tiles


UNION ALL

select 'Commercial' as therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'NET_SALES_BUDGET' as Metric_Name, net_sales_budget as Metric_Value1, '0' as Metric_Value2
from eisai_prod_db.stg_net_sales_budget_tiles

UNION ALL

select therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'TRX_ACTUALS' as Metric_Name, curr_trx as Metric_Value1, prev_trx as Metric_Value2
from eisai_prod_db.trx_growth_tbl

UNION ALL

select 'Commercial' as therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'NET_SALES' as Metric_Name, curr_sales as Metric_Value1, prev_sales as Metric_Value2
from eisai_prod_db.sales_growth_tbl

UNION ALL

select therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'TRX_CONTRIBUTION' as Metric_Name, trx_contribution as Metric_Value1, '0' as Metric_Value2
from eisai_prod_db.trx_share_tbl

UNION ALL

select 'Commercial' as therapy_type, brand_id, brand, indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, 'SALES_CONTRIBUTION' as Metric_Name, sales_contribution as Metric_Value1, '0' as Metric_Value2
from eisai_prod_db.sales_share_tbl
);