drop table if exists eisai_prod_db.stg_net_sales_budget_tiles;
create table eisai_prod_db.stg_net_sales_budget_tiles AS

select total.*
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(
--MONTH

select Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, sum(Net_Sales_Budget) as Net_Sales_Budget
from

(
select Brand_ID, Brand, INDICATION, Year, mth, stg_net_sales.time_bckt_strt, stg_net_sales.time_bckt_end, stg_net_sales.time_bckt_cd, Net_Sales_Budget, Reporting_Visibility
from
(
select Brand_ID
,Brand
,INDICATION
,Year
,mth
,case when trim(Net_Sales_Budget) = '-' then '0'
else REPLACE(Net_Sales_Budget, ',', '') end as Net_Sales_Budget
,to_date(concat(year, substring(to_date(lpad(mth, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'CM' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_net_sales_budget_v2
) ldg_sales_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.sales_growth_tbl where time_bckt_cd = 'CM') stg_net_sales
on ldg_sales_budget.time_bckt_end BETWEEN stg_net_sales.time_bckt_strt and stg_net_sales.time_bckt_end
) as fnl
group by Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility


UNION ALL


--QUARTER

select Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, sum(Net_Sales_Budget) as Net_Sales_Budget
FROM

(select Brand_ID, Brand, INDICATION, Year, mth, stg_net_sales.time_bckt_strt, stg_net_sales.time_bckt_end, stg_net_sales.time_bckt_cd, Net_Sales_Budget, Reporting_Visibility
FROM
(
select Brand_ID
,Brand
,INDICATION
,Year
,mth
,case when trim(Net_Sales_Budget) = '-' then '0'
else REPLACE(Net_Sales_Budget, ',', '') end as Net_Sales_Budget
,to_date(concat(year, substring(to_date(lpad(mth, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'QTD' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_net_sales_budget_v2
) as ldg_sales_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.sales_growth_tbl where time_bckt_cd = 'QTD') stg_net_sales
on ldg_sales_budget.time_bckt_end BETWEEN stg_net_sales.time_bckt_strt and stg_net_sales.time_bckt_end
) as fnl
group by Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility


UNION ALL


--YTD

select Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility,  sum(Net_Sales_Budget) as Net_Sales_Budget
FROM

(select Brand_ID, Brand, INDICATION, Year, mth, stg_net_sales.time_bckt_strt, stg_net_sales.time_bckt_end, stg_net_sales.time_bckt_cd, Net_Sales_Budget, Reporting_Visibility
FROM
(select Brand_ID
,Brand
,INDICATION
,Year
,mth
,case when trim(Net_Sales_Budget) = '-' then '0'
else REPLACE(Net_Sales_Budget, ',', '') end as Net_Sales_Budget
,to_date(concat(year, substring(to_date(lpad(mth, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'YTD' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_net_sales_budget_v2
) as ldg_sales_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.sales_growth_tbl where time_bckt_cd = 'YTD') stg_net_sales
on ldg_sales_budget.time_bckt_end BETWEEN stg_net_sales.time_bckt_strt and stg_net_sales.time_bckt_end
) as fnl
group by Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility


) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Reporting_Visibility)) = 'Y';