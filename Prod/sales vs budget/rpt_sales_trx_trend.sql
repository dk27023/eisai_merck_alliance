drop table if exists eisai_prod_db.rpt_sales_trx_trend;
create table eisai_prod_db.rpt_sales_trx_trend AS

(
select Therapy_Type, brand_id, brand, indication, grouped_indication, year, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, trx_budget as Metric_Value, 'TRX_BUDGET' as Metric_Name
from eisai_prod_db.stg_trx_budget_trend

UNION ALL

select Therapy_Type, brand_id, brand, indication, grouped_indication, year, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, trx_actuals as Metric_Value, 'TRX_ACTUALS' as Metric_Name
from eisai_prod_db.stg_trx_actual_trend

UNION ALL

select 'Commercial' as Therapy_Type, brand_id, brand, indication, grouped_indication, year, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, net_sales_budget as Metric_Value, 'NET_SALES_BUDGET' as Metric_Name
from eisai_prod_db.stg_net_sales_budget_trend

UNION ALL

select 'Commercial' as Therapy_Type, brand_id, brand, indication, grouped_indication, year, time_bckt_strt, time_bckt_end, time_bckt_cd, reporting_visibility, org, net_sales as Metric_Value, 'NET_SALES' as Metric_Name
from eisai_prod_db.stg_net_sales_trend
);