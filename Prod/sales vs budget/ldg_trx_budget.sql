drop table if exists eisai_prod_db.ldg_trx_budget;
create table eisai_prod_db.ldg_trx_budget
(
Therapy_Type varchar(255)
,Brand_ID varchar(255)                                                                                  
,Brand varchar(255)                                                                                                                            
,INDICATION varchar(255)                                                                                 
,Year varchar(255)                                                                           
,Month varchar(255)   
,TRx_Budget varchar(255)       
,Reporting_Visibility  varchar(255)                                                
);

TRUNCATE table eisai_prod_db.ldg_trx_budget;

copy eisai_prod_db.ldg_trx_budget 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Dev/Sales_vs_Budget/Trx_Budget/Dec/2021_Budget_TRx_data.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;

select * from eisai_prod_db.ldg_trx_budget;

-----------

drop table if exists eisai_prod_db.ldg_trx_budget_v2;
create table eisai_prod_db.ldg_trx_budget_v2 AS

select Therapy_Type, Brand_ID, Brand, INDICATION
,case when month > 12 then year+1
else year end as year
,case when month > 12 then month-12
else month end as month
,TRx_Budget, Reporting_Visibility
FROM
(SELECT Therapy_Type
,Brand_ID
,Brand
,INDICATION
,cast(Year as int) as year
,cast(Month as int)+3 as month
,TRx_Budget
,Reporting_Visibility
from eisai_prod_db.ldg_trx_budget
);