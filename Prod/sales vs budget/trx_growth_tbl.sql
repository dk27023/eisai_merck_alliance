--Time Buckets-
drop table if exists eisai_prod_db.trx_tm_tbl;
create table eisai_prod_db.trx_tm_tbl as
select distinct year, month
,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt 
,to_date(substring(dateadd(day,-1,add_months(mth_strt,1)),1,10),'YYYY-MM-DD') as mth_end

,max(mth_end) over (partition by null) as curr_mth_end
,to_date(substring(dateadd(day,1,add_months(curr_mth_end,-1)),1,10),'YYYY-MM-DD') as curr_mth_strt
--,to_date((extract(year from curr_mth_end)||'-'||lpad(curr_mth_end,2,'00')||'-01'),'YYYY-MM-DD') as curr_mth_strt
,to_date(substring(add_months(curr_mth_end,-1),1,10),'YYYY-MM-DD') as prev_mth_end
,to_date(substring(add_months(curr_mth_strt,-1),1,10),'YYYY-MM-DD') as prev_mth_strt


,max(mth_end) over (partition by null) as curr_qtd_end
,extract(quarter from curr_qtd_end) as curr_qtr_num
,to_date((year||'-'||lpad(curr_qtr_num*3-2,2,'00')||'-01'),'YYYY-MM-DD') as curr_qtd_strt
,to_date(substring(add_months(curr_qtd_end,-3),1,10),'YYYY-MM-DD') as prev_qtd_end
,to_date(substring(add_months(curr_qtd_strt,-3),1,10),'YYYY-MM-DD') as prev_qtd_strt

,max(mth_end) over (partition by null) as curr_year_end
,to_date(extract(year from curr_year_end)||'-04-01','YYYY-MM-DD') as curr_year_strt
,to_date(substring(add_months(curr_year_end,-12),1,10),'YYYY-MM-DD') as prev_year_end 
,to_date(substring(add_months(curr_year_strt,-12),1,10),'YYYY-MM-DD') as prev_year_strt 

from eisai_prod_db.ldg_trx_actuals_v2;

-------Current Period TRx Actuals----

drop table if exists eisai_prod_db.curr_trx_tbl;
create table eisai_prod_db.curr_trx_tbl as

select therapy_type, brand_id,brand, total.indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end, trx_act
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select therapy_type,brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end,sum(replace(trx_actuals,',','')) as trx_act
from 
(
select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'QTD' as time_bckt_cd,curr_qtd_strt as time_bckt_strt, curr_qtd_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all 

select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'YTD' as time_bckt_cd,curr_year_strt as time_bckt_strt, curr_year_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all

select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'CM' as time_bckt_cd,curr_mth_strt as time_bckt_strt, curr_mth_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end
) as trx_tbl
--where reporting_visibility='Y'
group by therapy_type,brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Therapy_Type)) in ('COMMERCIAL', 'TOTAL');

-------------Previous Period TRx Actuals--------------

drop table if exists eisai_prod_db.prev_trx_tbl;
create table eisai_prod_db.prev_trx_tbl as


select therapy_type, brand_id,brand, total.indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end, trx_act
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select therapy_type,brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end,sum(replace(trx_actuals,',','')) as trx_act
from 

(
select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'QTD' as time_bckt_cd,prev_qtd_strt as time_bckt_strt, prev_qtd_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all 

select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'YTD' as time_bckt_cd,prev_year_strt as time_bckt_strt, prev_year_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all

select * 
from (select *,to_date((year||'-'||lpad(month,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_trx_actuals_v2) as trx
join (
select distinct 'CM' as time_bckt_cd,prev_mth_strt as time_bckt_strt, prev_mth_end  as time_bckt_end
from eisai_prod_db.trx_tm_tbl
) as tm
on trx.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end
) as trx_tbl

--where reporting_visibility='Y'
group by therapy_type,brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Therapy_Type)) in ('COMMERCIAL', 'TOTAL');

--------------TRX GROWTH-----------------


drop table if exists eisai_prod_db.trx_growth_tbl;
create table eisai_prod_db.trx_growth_tbl as

select 
coalesce(ct.therapy_type                 , pt.therapy_type              ) as  therapy_type               
,coalesce(ct.brand_id                 , pt.brand_id              ) as  brand_id               
,coalesce(ct.brand                    , pt.brand                 ) as  brand 
,coalesce(ct.grouped_indication       , pt.grouped_indication  ) as  grouped_indication                  
,coalesce(ct.indication               , pt.indication            ) as  indication             
,coalesce(ct.reporting_visibility     , pt.reporting_visibility  ) as  reporting_visibility   
,coalesce(ct.time_bckt_cd             , pt.time_bckt_cd          ) as  time_bckt_cd           
,coalesce(ct.time_bckt_strt           , pt.time_bckt_strt        ) as  time_bckt_strt         
,coalesce(ct.time_bckt_end            , pt.time_bckt_end         ) as  time_bckt_end   
,coalesce(ct.ORG            , pt.ORG         ) as  ORG      
 ,coalesce(ct.trx_act,0) as curr_trx
,coalesce(pt.trx_act,0) as prev_trx

from eisai_prod_db.curr_trx_tbl as ct
full join 
eisai_prod_db.prev_trx_tbl as pt
on  ct.therapy_type                  = pt.therapy_type                  
and ct.brand_id                  = pt.brand_id                  
and ct.brand                     = pt.brand 
and ct.grouped_indication 		 = pt.grouped_indication                    
and ct.indication                = pt.indication                
and ct.reporting_visibility      = pt.reporting_visibility      
and ct.time_bckt_cd              = pt.time_bckt_cd 
and ct.ORG						 = pt.ORG;

----------------------------------

drop table if exists eisai_prod_db.trx_share_tbl;
create table eisai_prod_db.trx_share_tbl as

select trx_growth.therapy_type, trx_growth.brand_id, trx_growth.brand, trx_growth.grouped_indication, trx_growth.indication, trx_growth.reporting_visibility, trx_growth.time_bckt_cd, trx_growth.time_bckt_strt, trx_growth.time_bckt_end, trx_growth.org, curr_trx, prev_trx, trx_all, cast(curr_trx/trx_all as decimal(22,7) ) as trx_contribution
FROM

(select * from eisai_prod_db.trx_growth_tbl) as trx_growth
join
(select distinct therapy_type, brand_id, brand, reporting_visibility, time_bckt_cd, time_bckt_strt, time_bckt_end, org
,sum(curr_trx) over (partition by brand_id, brand, grouped_indication, reporting_visibility, time_bckt_cd, time_bckt_strt, time_bckt_end, ORG) as trx_all
from eisai_prod_db.trx_growth_tbl
where upper(trim(grouped_indication)) = 'ALL'
) trx_cal
on
trx_growth.therapy_type 				= trx_cal.therapy_type
and trx_growth.brand_id 				= trx_cal.brand_id
and trx_growth.brand 				    = trx_cal.brand
and trx_growth.reporting_visibility     = trx_cal.reporting_visibility
and trx_growth.time_bckt_cd 		    = trx_cal.time_bckt_cd
and trx_growth.time_bckt_strt 		    = trx_cal.time_bckt_strt
and trx_growth.time_bckt_end 		    = trx_cal.time_bckt_end
and trx_growth.org 					    = trx_cal.org;