drop table if exists eisai_prod_db.stg_trx_budget_tiles;
create table eisai_prod_db.stg_trx_budget_tiles AS

select Therapy_Type, Brand_ID, Brand, total.INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, TRx_Budget
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(
--MONTH

select Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, sum(TRx_Budget) as TRx_Budget
from

(
select Therapy_Type, Brand_ID, Brand, INDICATION, Year, mth, stg_actuals.time_bckt_strt, stg_actuals.time_bckt_end, stg_actuals.time_bckt_cd, TRx_Budget, Reporting_Visibility
from
(
select Therapy_Type
,Brand_ID
,Brand
,INDICATION
,Year
,Month as mth
,REPLACE(TRx_Budget, ',', '') as TRx_Budget
,to_date(concat(year, substring(to_date(lpad(month, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'CM' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_trx_budget_v2
) ldg_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.trx_growth_tbl where time_bckt_cd = 'CM') stg_actuals
on ldg_budget.time_bckt_end BETWEEN stg_actuals.time_bckt_strt and stg_actuals.time_bckt_end
) as fnl
group by Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility



UNION ALL

--QUARTER

select Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, sum(TRx_Budget) as TRx_Budget
FROM

(select Therapy_Type, Brand_ID, Brand, INDICATION, Year, mth, stg_actuals.time_bckt_strt, stg_actuals.time_bckt_end, stg_actuals.time_bckt_cd, TRx_Budget, Reporting_Visibility
FROM
(
select Therapy_Type
,Brand_ID
,Brand
,INDICATION
,Year
,Month as mth
,REPLACE(TRx_Budget, ',', '') as TRx_Budget
,to_date(concat(year, substring(to_date(lpad(month, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'QTD' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_trx_budget_v2
) as ldg_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.trx_growth_tbl where time_bckt_cd = 'QTD') stg_actuals
on ldg_budget.time_bckt_end BETWEEN stg_actuals.time_bckt_strt and stg_actuals.time_bckt_end
) as fnl
group by Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility



UNION ALL

--YTD

select Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility,  sum(TRx_Budget) as TRx_Budget
FROM

(select Therapy_Type, Brand_ID, Brand, INDICATION, Year, mth, stg_actuals.time_bckt_strt, stg_actuals.time_bckt_end, stg_actuals.time_bckt_cd, TRx_Budget, Reporting_Visibility
FROM
(select Therapy_Type
,Brand_ID
,Brand
,INDICATION
,Year
,Month as mth
,REPLACE(TRx_Budget, ',', '') as TRx_Budget
,to_date(concat(year, substring(to_date(lpad(month, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,EXTRACT (QUARTER FROM time_bckt_strt) as Quarter
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,'YTD' as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_trx_budget_v2
) as ldg_budget
inner join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.trx_growth_tbl where time_bckt_cd = 'YTD') stg_actuals
on ldg_budget.time_bckt_end BETWEEN stg_actuals.time_bckt_strt and stg_actuals.time_bckt_end
) as fnl
group by Therapy_Type, Brand_ID, Brand, INDICATION, Year, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility


) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(therapy_type)) in ('COMMERCIAL', 'TOTAL');