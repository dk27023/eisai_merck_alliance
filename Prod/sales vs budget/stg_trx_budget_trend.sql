drop table if exists eisai_prod_db.stg_trx_budget_trend;
create table eisai_prod_db.stg_trx_budget_trend AS

select Therapy_Type, Brand_ID, Brand, total.INDICATION, Year, mth, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, TRx_Budget
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
FROM
(select Therapy_Type
,Brand_ID
,Brand
,INDICATION
,Year
,Month as mth
,REPLACE(TRx_Budget, ',', '') as TRx_Budget
,to_date(concat(year, substring(to_date(lpad(month, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,dense_rank () over (partition by Therapy_Type, Brand_ID, Brand, INDICATION order by Year, time_bckt_strt desc) as rank_no
,concat('M', rank_no) as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_trx_budget_v2
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Therapy_Type)) in ('COMMERCIAL', 'TOTAL');