--Time Buckets-
drop table if exists eisai_prod_db.sls_tm_tbl;
create table eisai_prod_db.sls_tm_tbl as
select distinct year, mth
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt 
,to_date(substring(dateadd(day,-1,add_months(mth_strt,1)),1,10),'YYYY-MM-DD') as mth_end

,max(mth_end) over (partition by null) as curr_mth_end
,to_date(substring(dateadd(day,1,add_months(curr_mth_end,-1)),1,10),'YYYY-MM-DD') as curr_mth_strt
--,to_date((extract(year from curr_mth_end)||'-'||lpad(curr_mth_end,2,'00')||'-01'),'YYYY-MM-DD') as curr_mth_strt
,to_date(substring(add_months(curr_mth_end,-1),1,10),'YYYY-MM-DD') as prev_mth_end
,to_date(substring(add_months(curr_mth_strt,-1),1,10),'YYYY-MM-DD') as prev_mth_strt


,max(mth_end) over (partition by null) as curr_qtd_end
,extract(quarter from curr_qtd_end) as curr_qtr_num
,to_date((year||'-'||lpad(curr_qtr_num*3-2,2,'00')||'-01'),'YYYY-MM-DD') as curr_qtd_strt
,to_date(substring(add_months(curr_qtd_end,-3),1,10),'YYYY-MM-DD') as prev_qtd_end
,to_date(substring(add_months(curr_qtd_strt,-3),1,10),'YYYY-MM-DD') as prev_qtd_strt

,max(mth_end) over (partition by null) as curr_year_end
,to_date(extract(year from curr_year_end)||'-04-01','YYYY-MM-DD') as curr_year_strt
,to_date(substring(add_months(curr_year_end,-12),1,10),'YYYY-MM-DD') as prev_year_end
,to_date(substring(add_months(curr_year_strt,-12),1,10),'YYYY-MM-DD') as prev_year_strt

from eisai_prod_db.ldg_net_sales_v2;

-------Current Period SALES Actuals----

drop table  if exists eisai_prod_db.curr_sales_tbl;
create table eisai_prod_db.curr_sales_tbl as

select brand_id,brand, total.indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end, net_sales
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end,sum(net_sales) as net_sales
from 
(
select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'QTD' as time_bckt_cd,curr_qtd_strt as time_bckt_strt, curr_qtd_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all 

select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'YTD' as time_bckt_cd,curr_year_strt as time_bckt_strt, curr_year_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all

select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'CM' as time_bckt_cd,curr_mth_strt as time_bckt_strt, curr_mth_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end
) as sales_tbl
where reporting_visibility='Y'
group by brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on UPPER(trim(total.indication)) = upper(trim(xref.indication));

-------------Previous Period sales Actuals--------------

drop table  if exists eisai_prod_db.prev_sales_tbl;
create table eisai_prod_db.prev_sales_tbl as


select brand_id,brand, total.indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end, net_sales
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end,sum(net_sales) as net_sales
from 

(
select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'QTD' as time_bckt_cd,prev_qtd_strt as time_bckt_strt, prev_qtd_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all 

select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'YTD' as time_bckt_cd,prev_year_strt as time_bckt_strt, prev_year_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end

union all

select * 
from (select Brand_ID, brand, indication, year, mth, Reporting_Visibility
,case when trim(Net_Sales) = '-' then '0' 
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date((year||'-'||lpad(mth,2,'00')||'-01'),'YYYY-MM-DD')  as mth_strt  from eisai_prod_db.ldg_net_sales_v2) as sales
join (
select distinct 'CM' as time_bckt_cd,prev_mth_strt as time_bckt_strt, prev_mth_end  as time_bckt_end
from eisai_prod_db.sls_tm_tbl
) as tm
on sales.mth_strt between tm.time_bckt_strt and  tm.time_bckt_end


) as sales_tbl

where reporting_visibility='Y'
group by brand_id,brand,indication,reporting_visibility,time_bckt_cd,time_bckt_strt,time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication));

--------------sales GROWTH-----------------


drop table  if exists eisai_prod_db.sales_growth_tbl;
create table eisai_prod_db.sales_growth_tbl as

select               
coalesce(ct.brand_id                 , pt.brand_id              ) as  brand_id               
,coalesce(ct.brand                    , pt.brand                 ) as  brand                  
,coalesce(ct.indication               , pt.indication            ) as  indication             
,coalesce(ct.reporting_visibility     , pt.reporting_visibility  ) as  reporting_visibility   
,coalesce(ct.time_bckt_cd             , pt.time_bckt_cd          ) as  time_bckt_cd           
,coalesce(ct.time_bckt_strt           , pt.time_bckt_strt        ) as  time_bckt_strt         
,coalesce(ct.time_bckt_end            , pt.time_bckt_end         ) as  time_bckt_end   
, coalesce(ct.grouped_indication            , pt.grouped_indication         ) as  grouped_indication 
,coalesce(ct.ORG            , pt.ORG         ) as  ORG          
 ,coalesce(ct.net_sales,0) as curr_sales
,coalesce(pt.net_sales,0) as prev_sales

from eisai_prod_db.curr_sales_tbl as ct
full join 
eisai_prod_db.prev_sales_tbl as pt
on  ct.brand_id                  = pt.brand_id                  
and ct.brand                     = pt.brand                     
and ct.indication                = pt.indication                
and ct.reporting_visibility      = pt.reporting_visibility      
and ct.time_bckt_cd              = pt.time_bckt_cd 
and ct.grouped_indication 		 = pt.grouped_indication
and ct.ORG						 = pt.ORG;

-------------------------------------------

drop table if exists eisai_prod_db.sales_share_tbl;
create table eisai_prod_db.sales_share_tbl as

select sales_growth.brand_id, sales_growth.brand, sales_growth.grouped_indication, sales_growth.indication, sales_growth.reporting_visibility, sales_growth.time_bckt_cd, sales_growth.time_bckt_strt, sales_growth.time_bckt_end, sales_growth.org, curr_sales, prev_sales, sales_all, cast(curr_sales/sales_all as decimal(22,7) ) as sales_contribution
FROM

(select * from eisai_prod_db.sales_growth_tbl) as sales_growth
join
(select distinct brand_id, brand, reporting_visibility, time_bckt_cd, time_bckt_strt, time_bckt_end, org
,sum(curr_sales) over (partition by brand_id, brand, grouped_indication, reporting_visibility, time_bckt_cd, time_bckt_strt, time_bckt_end, ORG) as sales_all
from eisai_prod_db.sales_growth_tbl
where upper(trim(grouped_indication)) = 'ALL'
) sales_cal
on
sales_growth.brand_id 					= sales_cal.brand_id
and sales_growth.brand 					= sales_cal.brand
and sales_growth.reporting_visibility 	= sales_cal.reporting_visibility
and sales_growth.time_bckt_cd 			= sales_cal.time_bckt_cd
and sales_growth.time_bckt_strt 		= sales_cal.time_bckt_strt
and sales_growth.time_bckt_end 			= sales_cal.time_bckt_end
and sales_growth.org 					= sales_cal.org;