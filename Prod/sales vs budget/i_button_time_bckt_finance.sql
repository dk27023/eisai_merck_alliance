drop table if exists eisai_prod_db.i_button_time_bckt_finance ;
create table  eisai_prod_db.i_button_time_bckt_finance as 

select distinct 'ANY' as indication, 'CM' as time_bckt, 'CM(Current Month)' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_sales_trx_tiles where time_bckt_cd = 'CM'

UNION ALL

select distinct 'ANY' as indication,'QTD' as time_bckt, 'QTD(Quarter to Date)' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_sales_trx_tiles where time_bckt_cd = 'QTD'

UNION ALL

select distinct 'ANY' as indication,'FYTD' as time_bckt, 'FYTD(Fiscal Year to Date)' as time_desc, to_date(time_bckt_strt,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_sales_trx_tiles where time_bckt_cd = 'YTD'

;