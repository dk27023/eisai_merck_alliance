drop table if exists eisai_prod_db.stg_net_sales_trend;
create table eisai_prod_db.stg_net_sales_trend AS

select Brand_ID, Brand, total.INDICATION, Year, mth, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, Net_Sales
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select Brand_ID, Brand, INDICATION, Year, mth, ldg_sales.time_bckt_strt, ldg_sales.time_bckt_end, stg_sales_budget.time_bckt_cd, Reporting_Visibility, Net_Sales
FROM
(select Brand_ID
,Brand
,INDICATION
,Year
,mth
,case when upper(trim(Net_Sales)) = '-' then '0'
else REPLACE(Net_Sales, ',', '') end as Net_Sales
,to_date(concat(year, substring(to_date(lpad(mth, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_net_sales_v2
) ldg_sales
right join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.stg_net_sales_budget_trend) stg_sales_budget
on ldg_sales.time_bckt_strt = stg_sales_budget.time_bckt_strt
and ldg_sales.time_bckt_end = stg_sales_budget.time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Reporting_Visibility)) = 'Y';