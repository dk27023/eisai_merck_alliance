create table eisai_prod_db.i_button_metric_finance as 
select distinct 
'TRx' as metric,'Total TRx done in a particular period' as description
union 
select distinct 'TRx Budget','Total TRx budget allotted for given time period'
union select distinct 'Net Sales','Total sales done in a particular period'
union select distinct 'Net Sales Budget','Total Net Sales budget allotted for given time period'
union select distinct 'Attainment', 'Total Net Sales or Trx over the Budget alloted for the given time period'
union select distinct 'Growth',' Change w.r.t the previous time period';