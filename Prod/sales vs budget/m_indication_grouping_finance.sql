drop table if exists eisai_prod_db.m_indication_grouping_finance;
create table eisai_prod_db.m_indication_grouping_finance
(
Grouped_Indication varchar(255)
,Indication varchar(255) 
);

TRUNCATE table eisai_prod_db.m_indication_grouping_finance;

copy eisai_prod_db.m_indication_grouping_finance 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Dev/Sales_vs_Budget/m_indication_grouping_finance.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;
