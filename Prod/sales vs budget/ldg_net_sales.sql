TRUNCATE table eisai_prod_db.ldg_net_sales;

copy eisai_prod_db.ldg_net_sales 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales_vs_Budget/Net_sales/Lenvima_Actual_Net_Sales_data.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;

drop table if exists eisai_prod_db.ldg_net_sales_v2;
create table eisai_prod_db.ldg_net_sales_v2 AS

select Brand_ID, Brand, INDICATION
,case when Mth > 12 then year+1
else year end as year
,case when Mth > 12 then Mth-12
else Mth end as Mth
,Net_Sales, Reporting_Visibility
FROM
(SELECT Brand_ID
,Brand
,INDICATION
,cast(Year as int) as year
,cast(Mth as int)+3 as Mth
,Net_Sales
,Reporting_Visibility
from eisai_prod_db.ldg_net_sales
);