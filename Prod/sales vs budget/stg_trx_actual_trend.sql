drop table if exists eisai_prod_db.stg_trx_actual_trend;
create table eisai_prod_db.stg_trx_actual_trend AS

select Therapy_Type, Brand_ID, Brand, total.INDICATION, Year, mth, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, TRx_Actuals
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
from
(select Therapy_Type, Brand_ID, Brand, INDICATION, Year, mth, ldg_trx.time_bckt_strt, ldg_trx.time_bckt_end, stg_trx_budget.time_bckt_cd, Reporting_Visibility, TRx_Actuals
FROM
(select Therapy_Type
,Brand_ID
,Brand
,INDICATION
,Year
,Month as mth
,REPLACE(TRx_Actuals, ',', '') as TRx_Actuals
,to_date(concat(year, substring(to_date(lpad(month, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_trx_actuals_v2
) ldg_trx
right join
(select distinct time_bckt_strt, time_bckt_end, time_bckt_cd from eisai_prod_db.stg_trx_budget_trend) stg_trx_budget
on ldg_trx.time_bckt_strt = stg_trx_budget.time_bckt_strt
and ldg_trx.time_bckt_end = stg_trx_budget.time_bckt_end
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Therapy_Type)) in ('COMMERCIAL', 'TOTAL');