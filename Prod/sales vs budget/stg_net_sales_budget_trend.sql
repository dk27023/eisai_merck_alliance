drop table if exists eisai_prod_db.stg_net_sales_budget_trend;
create table eisai_prod_db.stg_net_sales_budget_trend AS

select Brand_ID, Brand, total.INDICATION, Year, mth, time_bckt_strt, time_bckt_end, time_bckt_cd, Reporting_Visibility, Net_Sales_Budget
,COALESCE(xref.grouped_indication, total.indication) as grouped_indication
,'ALLIANCE' as org
FROM
(select Brand_ID
,Brand
,INDICATION
,Year
,mth
,case when upper(trim(Net_Sales_Budget)) = '-' then '0'
else REPLACE(Net_Sales_Budget, ',', '') end as Net_Sales_Budget
,to_date(concat(year, substring(to_date(lpad(mth, 2, '00'), 'MM', TRUE), 5, 6)), 'YYYY-MM-DD') as time_bckt_strt
,to_date( (add_months(time_bckt_strt,1)-1), 'YYYY-MM-DD') as time_bckt_end
,dense_rank () over (partition by Brand_ID, Brand order by Year, time_bckt_strt desc) as rank_no
,concat('M', rank_no) as time_bckt_cd
,COALESCE(Reporting_Visibility, 'N') as Reporting_Visibility
from eisai_prod_db.ldg_net_sales_budget_v2
) total
left join
(select * from eisai_prod_db.m_indication_grouping_finance) xref
on upper(trim(total.indication)) = upper(trim(xref.indication))
where upper(trim(Reporting_Visibility)) = 'Y';