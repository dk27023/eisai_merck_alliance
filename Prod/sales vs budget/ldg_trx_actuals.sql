TRUNCATE table eisai_prod_db.ldg_trx_actuals;

copy eisai_prod_db.ldg_trx_actuals 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Prod/Latest/Sales_vs_Budget/Trx/Lenvima_Actual_TRx_data.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;

drop table if exists eisai_prod_db.ldg_trx_actuals_v2;
create table eisai_prod_db.ldg_trx_actuals_v2 AS

select Therapy_Type, Brand_ID, Brand, INDICATION
,case when month > 12 then year+1
else year end as year
,case when month > 12 then month-12
else month end as month
,TRx_Actuals, Reporting_Visibility
FROM
(SELECT Therapy_Type
,Brand_ID
,Brand
,INDICATION
,cast(Year as int) as year
,cast(Month as int)+3 as month
,case when trim(TRx_Actuals) = '-' then '0'
else REPLACE(TRx_Actuals, ',', '') end as TRx_Actuals
,Reporting_Visibility
from eisai_prod_db.ldg_trx_actuals
where month <> ' '
)
;