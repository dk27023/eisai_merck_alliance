drop table if exists eisai_prod_db.stg_digital_mkt;
create table eisai_prod_db.stg_digital_mkt AS
select 
CNTRY_ID
,BRAND_ID
,BRAND_NAME
,INDICATION
,TIME_PERIOD
,to_date(TIME_PERIOD_START, 'MM/DD/YYYY', FALSE) as TIME_PERIOD_START
,to_date(TIME_PERIOD_END, 'MM/DD/YYYY', FALSE) as TIME_PERIOD_END
,cast(replace(TOTAL_TARGETS, ',', '') as decimal(22,7)) as TOTAL_TARGETS
,cast(replace(TOTAL_HCP_REACH, ',', '') as decimal (22,7)) as TOTAL_HCP_REACH
,cast(replace(HCP_REACH_PRCNTG, '%', '') as decimal (22,7)) as HCP_REACH_PRCNTG
,cast(replace(TOTAL_HCP_ENGAGE, ',', '') as decimal (22,7)) as TOTAL_HCP_ENGAGE
,cast(replace(HCP_ENGAGE_PRCNTG, '%', '') as decimal (22,7)) as HCP_ENGAGE_PRCNTG
,cast(AVG_FREQ_HCP as decimal (22,7)) as AVG_FREQ_HCP
,DATA_DLVRY_DATE
,DATA_RCVD_DATE
,cast(replace(SENT, ',', '') as decimal (22,7)) as SENT
,cast(replace(DELIVERED, ',', '') as decimal (22,7)) as DELIVERED
,cast(replace(OPEN_PRCNTG, '%', '') as decimal (22,7)) as OPEN_PRCNTG
,cast(replace(CLICK_PRCNTG, '%', '') as decimal (22,7)) as CLICK_PRCNTG

from eisai_prod_db.ldg_digital_mkt;
