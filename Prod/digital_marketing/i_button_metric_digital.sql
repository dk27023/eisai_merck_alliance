create table eisai_prod_db.i_button_metric_digital as 
select distinct 
'Total Target HCPs' as metric,'Total no. of target HCPs for a particular indication for given time period' as description
union 
select distinct 'Total HCPs Reached','Total no. of HCPs Reached out of the total targets'
union select distinct 'Avg. Frequency per HCP','Average no. of times a particular HCP is reached out'
union select distinct 'Emails Sent','Total no. of emails sent to HCPs'
union select distinct 'Emails Delivered', 'Total no. of emails deliered out of the sent emails'
union select distinct 'Open Rate%', 'Percentage of emails opened'
union select distinct 'Click Rate%', 'Percentage of emails clicked';