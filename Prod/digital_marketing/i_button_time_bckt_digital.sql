drop table if exists eisai_prod_db.i_button_time_bckt_digital ;
create table  eisai_prod_db.i_button_time_bckt_digital as 

select distinct 'ANY' as indication,'YTD' as time_bckt, 'YTD(Year to Date)' as time_desc, to_date(TIME_PERIOD_START,'YYYY-MM-DD') as time_bckt_strt, to_date(TIME_PERIOD_END,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.stg_digital_mkt where upper(TIME_PERIOD) = 'YTD'
;