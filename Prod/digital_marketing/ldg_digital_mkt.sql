drop table if exists eisai_prod_db.ldg_digital_mkt;
create table eisai_prod_db.ldg_digital_mkt
(
CNTRY_ID varchar(255)                                                                                  
,BRAND_ID varchar(255)                                                                                                                            
,BRAND_NAME varchar(255)                                                                                 
,INDICATION varchar(255)                                                                           
,TIME_PERIOD varchar(255)   
,TIME_PERIOD_START varchar(255)               
,TIME_PERIOD_END varchar(255)
,TOTAL_TARGETS varchar(255)
,TOTAL_HCP_REACH varchar(255)
,HCP_REACH_PRCNTG varchar(255)
,TOTAL_HCP_ENGAGE varchar(255)
,HCP_ENGAGE_PRCNTG varchar(255)
,AVG_FREQ_HCP float8
,DATA_DLVRY_DATE varchar(255)
,DATA_RCVD_DATE varchar(255)
,SENT varchar(255)
,DELIVERED varchar(255)
,OPEN_PRCNTG varchar(255)
,CLICK_PRCNTG varchar(255)                   
);

TRUNCATE table eisai_prod_db.ldg_digital_mkt;

copy eisai_prod_db.ldg_digital_mkt 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Dev/Digital_Marketing/December_2021/December_2021.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;


select * from eisai_prod_db.ldg_digital_mkt;
