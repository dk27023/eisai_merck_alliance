drop table if exists eisai_prod_db.idl_eisai_sales_ind_grp;
create table eisai_prod_db.idl_eisai_sales_ind_grp as 
select * from 
(
select 
org,cntry_id,src_cd,channel,product_id,prod_name,brand_id,brand_name,i_xref.grouped_indication, sls_ind.indication,time_bckt_freq,time_bckt_cd,time_bckt_nm,time_bckt_strt,time_bckt_end,recency
,max(trx_goals) as trx_goals  
,max(days_of_therapy) as days_of_therapy 
,sum(curr_trx) as curr_trx
,sum(curr_nrx) as curr_nrx
,sum(prev_trx) as prev_trx
,sum(prev_nrx) as prev_nrx
,sum(trx_diff) as trx_diff
,sum(nrx_diff) as nrx_diff

from 
(
select 
org,cntry_id,src_cd,channel,product_id,prod_name,brand_id,brand_name,grouped_indication as indication,time_bckt_freq,time_bckt_cd,time_bckt_nm,time_bckt_strt,time_bckt_end,recency,trx_goals,days_of_therapy,curr_trx,curr_nrx,prev_trx,prev_nrx,trx_diff,nrx_diff
from 

(
select sls.*,xref.grouped_indication
from eisai_prod_db.idl_eisai_sales as sls
left join eisai_prod_db.m_indication_grouping as xref
on xref.indication=sls.indication
) as sls_ind_1
) as sls_ind
left join eisai_prod_db.m_indication_grouping as i_xref
on i_xref.indication=sls_ind.indication

group by org,cntry_id,src_cd,channel,product_id,prod_name,brand_id,brand_name,i_xref.grouped_indication, sls_ind.indication,time_bckt_freq,time_bckt_cd,time_bckt_nm,time_bckt_strt,time_bckt_end,recency
) as sls_int;


drop table if exists eisai_prod_db.idl_eisai_writer_ind_grp;
create table eisai_prod_db.idl_eisai_writer_ind_grp as 
select * from 
(
select 
cntry_id,src_cd,product_id,prod_name,brand_id,brand_name,grouped_indication,wrtr.indication,time_period,period_end_date,new_writer,repeat_writer,total_writer,new_writer_growth,total_writer_growth,data_dlvry_date,data_rcvd_date

from eisai_prod_db.ldg_sales_writer_eisai as wrtr
left join eisai_prod_db.m_indication_grouping as xref
on xref.indication=wrtr.indication
) as wrtr_int;


drop table if exists eisai_prod_db.idl_eisai_sales_writer;
create table eisai_prod_db.idl_eisai_sales_writer as select * from 
(
select sls.*,new_writer,	repeat_writer,	total_writer,	new_writer_growth,	total_writer_growth 
 
from eisai_prod_db.idl_eisai_sales_ind_grp as sls
left join 
(
SELECT * ,
case when time_period='LTD' then 'LTD (W)'
when time_period='CM' then 'M1'
when time_period='CW' then 'WTD'
when time_period='QTD' then 'QTD1 (W)'
when time_period='R13W' then 'R13W'
when time_period='R4W' then 'R4W'
when time_period='YTD' then 'YTD (W)'
else time_period end as sls_time_period
from eisai_prod_db.idl_eisai_writer_ind_grp
) as wrtr_mod

on 
((wrtr_mod.sls_time_period=sls.time_bckt_cd and  wrtr_mod.sls_time_period<>'M1')
or (upper(wrtr_mod.sls_time_period)='WEEKLY' and upper(wrtr_mod.sls_time_period)=sls.time_bckt_freq and to_date(wrtr_mod.period_end_date, 'MM/DD/YYYY', FALSE)=sls.time_bckt_end )
or (upper(wrtr_mod.sls_time_period)='MONTHLY' and upper(wrtr_mod.sls_time_period)=sls.time_bckt_freq and to_date(wrtr_mod.period_end_date, 'MM/DD/YYYY', FALSE)=sls.time_bckt_strt )
) 
and wrtr_mod.indication= sls.indication
and wrtr_mod.grouped_indication= sls.grouped_indication
and upper(wrtr_mod.src_cd)= upper(sls.src_cd)
) as sls_wrtr;

