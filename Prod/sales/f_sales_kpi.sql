drop table if exists eisai_prod_db.f_eisai_sales_prev_time_prd;
create table  eisai_prod_db.f_eisai_sales_prev_time_prd as 
select
'EISAI' as ORG, A.*, B.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_sales_kpi_eisai) A
left join (select distinct  recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where source = 'Sales') B
on
A.split_week_end between B.time_bckt_strt
and B.time_bckt_end
where B.time_bckt_cd in ('R4W(P)', 'R13W(P)', 'R12M(P)', 'WTD(P)', 'MTD(P)', 'QTD2 (W)', 'YTD2 (W)', 'M2');


drop table if exists eisai_prod_db.f_eisai_sales_crnt_time_prd;
create table  eisai_prod_db.f_eisai_sales_crnt_time_prd as 
select * from 
(
select
'EISAI' as ORG, A.*, B.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_sales_kpi_eisai ) A
left join (select distinct  recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where source = 'Sales') B
on
A.split_week_end between B.time_bckt_strt
and B.time_bckt_end
where B.time_bckt_cd in ('R4W', 'R13W', 'R12M', 'WTD', 'MTD', 'QTD1 (W)', 'YTD (W)', 'W1' ,'W2' ,'W3' ,'W4' ,'W5' ,'W6' ,'W7' ,'W8' ,'W9' ,'W10' ,'W11' ,'W12' ,'W13' ,'W14' ,'W15' ,'W16' ,'W17' ,'W18' ,'W19' ,'W20' ,'W21' ,'W22' ,'W23' ,'W24' ,'W25' ,'W26', 'M1' ,'M2' ,'M3' ,'M4' ,'M5' ,'M6' ,'M7' ,'M8' ,'M9' ,'M10' ,'M11' ,'M12' ,'M13' ,'M14' ,'M15' ,'M16' ,'M17' ,'M18' ,'M19' ,'M20' ,'M21' ,'M22' ,'M23' ,'M24')

union all
 
select 
'EISAI' as ORG, A.*, B.*
from (select *, to_date(split_week_end_date, 'MM/DD/YYYY', FALSE) as split_week_end from eisai_prod_db.ldg_sales_kpi_eisai ) A 
left join (select distinct  recency, to_date(substring(time_bckt_strt, 1, 10), 'YYYY-MM-DD') as time_bckt_strt, to_date(substring(time_bckt_end, 1, 10), 'YYYY-MM-DD') as time_bckt_end, time_bckt_nm, time_bckt_cd, time_bckt_freq, indication as time_bckt_indication from eisai_prod_db.d_time_bckt where source = 'Sales') B
on A.split_week_end between B.time_bckt_strt and B.time_bckt_end 
and A.indication = B.time_bckt_indication
where B.time_bckt_cd = 'LTD (W)'
) as sls;