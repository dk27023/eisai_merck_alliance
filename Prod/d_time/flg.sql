DROP TABLE IF EXISTS eisai_prod_db.flg;

CREATE TABLE eisai_prod_db.flg  as

select  case when max(cast(cal_dt as timestamp))=cast(a.var_val as timestamp) then 1 else 0 end flg_M
 from eisai_prod_db.ctl_job_run a 
 inner join eisai_prod_db.d_time d  
 on   to_char(to_date(var_val,'YYYY-MM-DD', FALSE),'YYYYMM')=to_char(to_date(d.cal_mth_id_yyyymm,'YYYYMM',FALSE),'YYYYMM')
group by a.var_val;

