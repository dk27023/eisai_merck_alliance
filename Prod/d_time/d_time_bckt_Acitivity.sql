drop table if exists eisai_prod_db.d_time_bckt_Activity;
create table eisai_prod_db.d_time_bckt_Activity as 
select * from 
(
select wkly_rnk.*
,CONCAT('WEEK',RECENCY) as time_bckt_nm 
,CONCAT('W',RECENCY) as time_bckt_cd
,'ANY' as indication
from 
(
select wkly.*,rank() over (order by  time_bckt_strt desc ) as recency
from 
(select distinct 'Activity' as source, 'WEEKLY' as time_bckt_freq, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end
,c.cycl_time_id
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')

between to_date(dateadd(day,-107*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
 and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
) as wkly
) as wkly_rnk

UNION ALL

select mnthly_rnk.*
,CONCAT('MONTH ',RECENCY) as time_bckt_nm 
,CONCAT('M',RECENCY) as time_bckt_cd
,'ANY' as indication
from 
(
select mnthly.*,rank() over (order by  time_bckt_strt desc ) as recency
from 
(
select distinct 'Activity' as source, 'MONTHLY' as time_bckt_freq, d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') 
between to_date(dateadd(month,(-36+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00')   
) as mnthly
) as mnthly_rnk

UNION ALL

select 'Activity' as source,'R13W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id
,1 as recency
,'ROLLING 13 WEEKS' as time_bckt_nm
,'R13W' as time_bckt_cd

,'ANY' as indication
from
(select distinct 'R13W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.d_time d inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-12*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)
group by cycl_time_id

UNION ALL

select 'Activity' as source,'R13W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id
,1 as recency
,'ROLLING 13 WEEKS PREVIOUS' as time_bckt_nm
,'R13W(P)' as time_bckt_cd

,'ANY' as indication
from
(select distinct 'R13W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.d_time d inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-25*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and  to_date(dateadd(day,-13*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
)
group by cycl_time_id

UNION ALL

select 'Activity' as source,'R4W' as time_bckt_freq
,cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 4 WEEKS' as time_bckt_nm
,'R4W' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R4W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.d_time d inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-3*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)
group by cycl_time_id

UNION ALL

select 'Activity' as source,'R4W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 4 WEEKS PREVIOUS' as time_bckt_nm
,'R4W(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R4W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.d_time d inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-7*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(dateadd(day,-4*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
)
group by cycl_time_id

UNION ALL

select 'Activity' as source,'R12M' as time_bckt_freq,
 cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 12 MONTHS' as time_bckt_nm
,'R12M' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R12M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-12+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id

UNION ALL

select'Activity' as source,'R12M' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 12 MONTHS PREVIOUS' as time_bckt_nm
,'R12M(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R12M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-24+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-13+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id

UNION ALL

select distinct source,time_bckt_freq, time_bckt_strt, b.cal_qtr_enddt as x
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication  from 
(
SELECT 'Activity' as source, 'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,  dateadd(month,-3,time_bckt_strt) AS time_bckt_strt , cycl_time_id ,
2 AS recency ,
'PREVIOUS QUARTER WEEKLY' AS time_bckt_nm,
'QTD2 (W)' AS time_bckt_cd,
'ANY' indication
,cal_wk_num_of_qtr
FROM 
(select 'Activity' as source,'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,
  CAL_QTR_STRTDT as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end, cycl_time_id 
,1 as recency,'CURRENT QUARTER WEEKLY' as time_bckt_nm,'QTD1 (W)' as time_bckt_cd,
'ANY' as indication ,cal_wk_num_of_qtr
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 
) as QTD 
)a left join eisai_prod_db.d_time b 
on a.time_bckt_strt=b.cal_qtr_strtdt
and a.cal_wk_num_of_qtr=b.cal_wk_num_of_qtr

UNION ALL

select distinct source,time_bckt_freq, time_bckt_strt
,b.cal_mth_enddt as x
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication  from 
(
SELECT 'Activity' as source,'MONTH TO DATE PREVIOUS' AS time_bckt_freq, dateadd(month,-1,time_bckt_strt) AS time_bckt_strt ,cycl_time_id, 
2 AS recency ,
'MONTH TO DATE PREVIOUS' AS time_bckt_nm, 
'MTD(P)' AS time_bckt_cd,
'ANY' as indication , cal_day_of_mth
FROM 
(
select 'Activity' as source,'MONTH TO DATE PREVIOUS' AS time_bckt_freq, cal_mth_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end
,cycl_time_id,1 as recency,'MONTH TO DATE' as time_bckt_nm,'MTD' as time_bckt_cd,'ANY' as indication
,cal_day_of_mth    
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
) as MTD 
)a left join eisai_prod_db.d_time b 
on a.time_bckt_strt=b.cal_mth_strtdt
and a.cal_day_of_mth=b.cal_day_of_mth

UNION ALL

select distinct source,time_bckt_freq, time_bckt_strt
,b.cal_wk_enddt as x
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication from 
(
SELECT 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq, dateadd(year,-1,time_bckt_strt) AS time_bckt_strt ,cycl_time_id,
2 AS recency ,
'PREVIOUS YEAR TO DATE WEEKLY' AS time_bckt_nm, -- Change as per MTD
'YTD2 (W)' AS time_bckt_cd,'ANY' as indication, cal_day_of_yr  
FROM 
(
select 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq, cal_yr_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end
, cycl_time_id,1 as recency,'PREVIOUS YEAR TO DATE WEEKLY' as time_bckt_nm,'YTD2 (W)' as time_bckt_cd, 'ANY' as indication, cal_day_of_yr   
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
) as YTD
)a left join eisai_prod_db.d_time b 
on a.time_bckt_strt=b.cal_yr_strtdt
and a.cal_day_of_yr=b.cal_day_of_yr

UNION ALL

select distinct source,time_bckt_freq, time_bckt_strt,time_bckt_end, cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication from
( select 
case 
when datepart(year,dateadd(month, -3,  to_date(activity_feed,'YYYY-MM-DD', FALSE))) = datepart(year,dateadd(month, 0,  to_date(activity_feed,'YYYY-MM-DD', FALSE)))
then to_date(dateadd(month, 3, cal_yr_strtdt),'YYYY-MM-DD 00:00:00')
else to_date(dateadd(year,-1,dateadd(month, 3, cal_yr_strtdt)),'YYYY-MM-DD 00:00:00')end as time_bckt_strt,
'Activity' as source,'FISCAL YEAR TO DATE (WEEKLY)' as time_bckt_freq
, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end, cycl_time_id 
,1 as recency,'FISCAL YEAR TO DATE WEEKLY' as time_bckt_nm,'FYTD1 (W)' as time_bckt_cd, 'ANY' as indication  
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)

UNION ALL

select 'Activity' as source,'LAUNCH TO DATE (WEEKLY)' as time_bckt_freq,
'2021-07-23' as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'LAUNCH TO DATE WEEKLY' as time_bckt_nm
,'LTD (W)' as time_bckt_cd
,'EC' as indication 
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
 
UNION ALL

select 'Activity' as source,'LAUNCH TO DATE (WEEKLY)' as time_bckt_freq,
'2021-08-12' as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'LAUNCH TO DATE WEEKLY' as time_bckt_nm
,'LTD (W)' as time_bckt_cd
,'RCC' as indication 
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 

UNION ALL

select 'Activity' as source,'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,
CAL_QTR_STRTDT as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end,
cycl_time_id 
,1 as recency,'CURRENT QUARTER WEEKLY' as time_bckt_nm,'QTD1 (W)' as time_bckt_cd,
'ANY' as indication    
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 

UNION ALL

select 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq,
 cal_yr_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end,
 cycl_time_id 
,1 as recency,'YEAR TO DATE WEEKLY' as time_bckt_nm,'YTD (W)' as time_bckt_cd,
'ANY' as indication
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 

UNION ALL

select 'Activity' as source,'CURRENT MONTH' as time_bckt_freq,
 cal_mth_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end , cycl_time_id
,1 as recency,'MONTH TO DATE' as time_bckt_nm,'MTD' as time_bckt_cd,
'ANY' as indication 
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)

UNION ALL

select 'Activity' as source,'CURRENT WEEK' as time_bckt_freq,
 cal_wk_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end , cycl_time_id
,1 as recency,'WEEK TO DATE' as time_bckt_nm,'WTD' as time_bckt_cd,
'ANY' as indication 
from  eisai_prod_db.d_time d 
join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)


UNION ALL
--ROLIING 6 MONTHS

select 'Activity' as source,'R6M' as time_bckt_freq,
 cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 6 MONTHS' as time_bckt_nm
,'R6M' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R6M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-6+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id


UNION ALL

--ROLLING 3 MONTHS

select 'Activity' as source,'R3M' as time_bckt_freq,
 cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 3 MONTHS' as time_bckt_nm
,'R3M' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R3M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-3+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id


UNION ALL

--ROLLING 6 MONTHS PREVIOUS

select 'Activity' as source,'R6M' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 6 MONTHS PREVIOUS' as time_bckt_nm
,'R6M(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R6M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-12+flg.flg_m),to_date(sales_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-7+flg.flg_m),to_date(sales_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id

UNION ALL
--ROLLING 3 MONTHS PREVIOUS

select 'Activity' as source,'R3M' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 3 MONTHS PREVIOUS' as time_bckt_nm
,'R3M(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R3M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_prod_db.flg as flg
cross join  eisai_prod_db.d_time d  
inner join eisai_prod_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-6+flg.flg_m),to_date(sales_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-4+flg.flg_m),to_date(sales_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id
);

