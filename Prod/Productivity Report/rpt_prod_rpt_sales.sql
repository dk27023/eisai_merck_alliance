drop table if exists eisai_prod_db.rpt_prod_rpt_sales;
create table eisai_prod_db.rpt_prod_rpt_sales AS


select org, hcp_id, brand_id, brand_name, iixref.grouped_indication, fnl.indication, time_bckt_freq, time_bckt_cd, time_bckt_start, time_bckt_end, curr_Trx, prev_Trx, curr_Nrx, prev_Nrx
FROM

(select org_target as org
,sales.indication, sales.hcp_id, brand_id, initcap(brand_name) as brand_name, time_bckt_freq, time_bckt_cd, time_bckt_start, time_bckt_end, curr_Trx, prev_Trx, curr_Nrx, prev_Nrx
from

(select ORG, INDICATION, lpad(hcp_id,7,'0000000') as hcp_id, brand_id, brand_name, time_bckt_freq, time_bckt_cd
,TO_DATE(time_bckt_start, 'YYYY/MM/DD') as time_bckt_start
,TO_DATE(time_bckt_end, 'YYYY/MM/DD') as time_bckt_end
,cast(curr_Trx as decimal(22,7)) as curr_Trx, cast(prev_Trx as decimal(22,7)) as prev_Trx
,cast(curr_Nrx as decimal (22,7)) as curr_Nrx, cast(prev_Nrx as decimal(22,7)) as prev_Nrx from eisai_prod_db.ldg_prod_rpt_sales) as sales

join

(
select npi_id, ims_id, segment, trgt_ind.indication, ixref.grouped_indication, early_adopter, org_target
from
(
select npi_id, ims_id, segment, xref.grouped_indication as indication, early_adopter, org_target from eisai_prod_db.stg_call_targets as trgt
left join eisai_prod_db.m_indication_grouping as xref
on upper(trim(trgt.indication)) = upper(trim(xref.indication))
) trgt_ind
left join eisai_prod_db.m_indication_grouping as ixref
on upper(trim(trgt_ind.indication)) = upper(trim(ixref.indication))
) as targets
ON
upper(trim(sales.indication)) = upper(trim(targets.grouped_indication))
and sales.hcp_id = targets.ims_id
) as fnl

left join eisai_prod_db.m_indication_grouping as iixref
on upper(trim(fnl.indication)) = upper(trim(iixref.indication));