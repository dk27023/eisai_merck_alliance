create table eisai_prod_db.i_button_prod_report as 
select distinct 'Details Attainment%' as metric,'Total details (targets + non targets) / Details goal' as description
union select distinct 'Target Reach %','Count of distinct Target HCPs called upon/ Total Target HCPs'
union select distinct 'Frequency','Total Details/ Distinct HCPs'
union select distinct 'TRx','Sum of new prescriptions'
union select distinct 'TRx Growth','Change in TRx for the brand over selected time period'
union select distinct 'NRx','Sum of new prescriptions'
union select distinct 'NRx Growth','Change in NRx for the brand over selected time period';