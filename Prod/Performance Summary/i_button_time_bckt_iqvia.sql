drop table if exists eisai_prod_db.i_button_time_bckt_iqvia ;
create table  eisai_prod_db.i_button_time_bckt_iqvia as 

select distinct 'ANY' as indication, 'CM' as time_bckt, 'CM(Current Full Month)' as time_desc, to_date(TIME_BCKT_START,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_iqvia_report where time_bckt_cd = 'M1'

UNION ALL

select distinct 'ANY' as indication,'QTD' as time_bckt, 'QTD(Quarter to Date)' as time_desc, to_date(TIME_BCKT_START,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_iqvia_report where time_bckt_cd = 'QTD'

UNION ALL

select distinct 'ANY' as indication,'YTD' as time_bckt, 'YTD(Year to Date)' as time_desc, to_date(TIME_BCKT_START,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_iqvia_report where time_bckt_cd = 'YTD'

UNION ALL

select distinct 'ANY' as indication,'C12M' as time_bckt, 'C12M(Rolling 12 Months)' as time_desc, to_date(TIME_BCKT_START,'YYYY-MM-DD') as time_bckt_strt, to_date(time_bckt_end,'YYYY-MM-DD') as time_bckt_end from eisai_prod_db.rpt_iqvia_report where time_bckt_cd = 'C12M'
;