drop table if exists eisai_prod_db.iqvia_tm_tbl;
create table eisai_prod_db.iqvia_tm_tbl as

select distinct 

--max(to_date(TIME_BCKT_END, 'DD-Mon-YY')) over (PARTITION by NULL) as curr_mth_end
--,to_date(substring(dateadd(day,1,add_months(curr_mth_end,-1)),1,10),'YYYY-MM-DD') as curr_mth_strt
--,to_date(substring(add_months(curr_mth_end,-1),1,10),'YYYY-MM-DD') as prev_mth_end
--,to_date(substring(add_months(curr_mth_strt,-1),1,10),'YYYY-MM-DD') as prev_mth_strt

max(to_date(TIME_BCKT_END, 'DD-Mon-YY')) over (partition by null) as curr_qtd_end
,extract(quarter from curr_qtd_end) as curr_qtr_num
,to_date((substring(curr_qtd_end,1,4)||'-'||lpad(curr_qtr_num*3-2,2,'00')||'-01'),'YYYY-MM-DD') as curr_qtd_strt
,to_date(substring(add_months(curr_qtd_end,-3),1,10),'YYYY-MM-DD') as prev_qtd_end
,to_date(substring(add_months(curr_qtd_strt,-3),1,10),'YYYY-MM-DD') as prev_qtd_strt

,max(to_date(TIME_BCKT_END, 'DD-Mon-YY')) over (partition by null) as curr_year_end
,to_date(extract(year from curr_year_end)||'-01-01','YYYY-MM-DD') as curr_year_strt
,to_date(substring(add_months(curr_year_end,-12),1,10),'YYYY-MM-DD') as prev_year_end
,to_date(substring(add_months(curr_year_strt,-12),1,10),'YYYY-MM-DD') as prev_year_strt

,max(to_date(TIME_BCKT_END, 'DD-Mon-YY')) over (partition by null) as curr_R12M_end
,to_date(substring(dateadd(day,1, add_months(curr_R12M_end,-12)),1,10), 'YYYY-MM-DD') as curr_R12M_strt
,to_date(substring(add_months(curr_R12M_end,-12),1,10),'YYYY-MM-DD') as prev_R12M_end
,to_date(substring(add_months(curr_R12M_strt,-12),1,10),'YYYY-MM-DD') as prev_R12M_strt
from eisai_prod_db.ldg_iqvia_report;


------------CURR----------------------

drop table  if exists eisai_prod_db.curr_iqvia_tbl;
create table eisai_prod_db.curr_iqvia_tbl as

select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end, TOT_PATIENTS, cast(class_total/TOT_PATIENTS*100 as decimal(22,7)) as class_share, cast(brand_total/class_total*100 as decimal(22,7)) as brand_share, class_total, brand_total
FROM

(
--QTD
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total
from eisai_prod_db.ldg_iqvia_report) as curr_iqvia
join
(
select distinct 'QTD' as time_bckt_cd, curr_qtd_strt as time_bckt_strt, curr_qtd_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on curr_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end 

UNION ALL

--YTD
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total
from eisai_prod_db.ldg_iqvia_report) as curr_iqvia
join
(
select distinct 'YTD' as time_bckt_cd, curr_year_strt as time_bckt_strt, curr_year_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on curr_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end 

UNION ALL

--R12M
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total
from eisai_prod_db.ldg_iqvia_report) as curr_iqvia
join
(
select distinct 'C12M' as time_bckt_cd, curr_R12M_strt as time_bckt_strt, curr_R12M_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on curr_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end 
);


----------PREV---------------------


drop table  if exists eisai_prod_db.prev_iqvia_tbl;
create table eisai_prod_db.prev_iqvia_tbl as

select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end, TOT_PATIENTS, cast(class_total/TOT_PATIENTS*100 as decimal(22,7)) as class_share, cast(brand_total/class_total*100 as decimal(22,7)) as brand_share, class_total, brand_total, data_strt_date
FROM

(
--QTD
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
,data_strt_date
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total, min(to_date(TIME_BCKT_START, 'DD-Mon-YY')) over (partition by null) as data_strt_date
from eisai_prod_db.ldg_iqvia_report) as prev_iqvia
join
(
select distinct 'QTD' as time_bckt_cd, prev_qtd_strt as time_bckt_strt, prev_qtd_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on prev_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end, data_strt_date

UNION ALL

--YTD
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
,data_strt_date
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total, min(to_date(TIME_BCKT_START, 'DD-Mon-YY')) over (partition by null) as data_strt_date
from eisai_prod_db.ldg_iqvia_report) as prev_iqvia
join
(
select distinct 'YTD' as time_bckt_cd, prev_year_strt as time_bckt_strt, prev_year_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on prev_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end, data_strt_date

UNION ALL

--R12M
select Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end
,sum(TOT_PATIENTS) as TOT_PATIENTS
,sum(class_total) as class_total, sum(brand_total) as brand_total
,data_strt_date
FROM
 
(select distinct Brand_ID, Brand_name, case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication, UPPER(Line_of_Therapy) as Line_of_Therapy, to_date(TIME_BCKT_START, 'DD-Mon-YY') as TIME_BCKT_START, replace(TOT_PATIENTS,',','') as TOT_PATIENTS, replace(class_total,',','') as class_total, replace(brand_total,',','') as brand_total, min(to_date(TIME_BCKT_START, 'DD-Mon-YY')) over (partition by null) as data_strt_date
from eisai_prod_db.ldg_iqvia_report) as prev_iqvia
join
(
select distinct 'C12M' as time_bckt_cd, prev_R12M_strt as time_bckt_strt, prev_R12M_end  as time_bckt_end
from eisai_prod_db.iqvia_tm_tbl
) as tm
on prev_iqvia.TIME_BCKT_START between tm.time_bckt_strt and tm.time_bckt_end
group by Brand_ID, Brand_name, Indication, Line_of_Therapy, time_bckt_cd, time_bckt_strt, time_bckt_end, data_strt_date
);



------------CHANGE-------------------------


drop table  if exists eisai_prod_db.rpt_iqvia_report;
create table eisai_prod_db.rpt_iqvia_report as

select               
coalesce(ct.brand_id                 , pt.brand_id              ) as  brand_id               
,coalesce(ct.Brand_name              , pt.Brand_name            ) as  brand                  
,coalesce(ct.indication              , pt.indication            ) as  indication             
,coalesce(ct.Line_of_Therapy     	 , pt.Line_of_Therapy  		) as  Line_of_Therapy   
,coalesce(ct.time_bckt_cd            , pt.time_bckt_cd          ) as  time_bckt_cd           
,coalesce(ct.time_bckt_strt          , pt.time_bckt_strt        ) as  time_bckt_strt         
,coalesce(ct.time_bckt_end           , pt.time_bckt_end         ) as  time_bckt_end   
,coalesce(ct.TOT_PATIENTS) as TOT_PATIENTS
,case when pt.time_bckt_strt > pt.data_strt_date then cast((ct.TOT_PATIENTS-pt.TOT_PATIENTS)*100/pt.TOT_PATIENTS as decimal(22,7)) 
else '0' end as TOT_PATIENTS_CHANGE

,coalesce(ct.class_share) as class_share
,coalesce(ct.brand_share) as brand_share

,case when pt.time_bckt_strt > pt.data_strt_date then cast(ct.class_share-pt.class_share as decimal(22,7)) 
else '0' end as class_share_change
,case when pt.time_bckt_strt > pt.data_strt_date then cast(ct.brand_share-pt.brand_share as decimal(22,7)) 
else '0' end as brand_share_change

,coalesce(ct.class_total) as class_total
,coalesce(ct.brand_total) as brand_total



from eisai_prod_db.curr_iqvia_tbl as ct
full join 
eisai_prod_db.prev_iqvia_tbl as pt
on  ct.brand_id                  = pt.brand_id                  
and ct.Brand_name                = pt.Brand_name                     
and ct.indication                = pt.indication                
and ct.Line_of_Therapy      	 = pt.Line_of_Therapy      
and ct.time_bckt_cd              = pt.time_bckt_cd

UNION ALL

select  Brand_ID, Brand_name 
,case when upper(trim(Indication)) = 'ENDO' then 'EC' else Indication end as Indication
,UPPER(trim(Line_of_Therapy)) as Line_of_Therapy, time_bckt_cd, to_date(time_bckt_start, 'DD-Mon-YY') as time_bckt_start, to_date(time_bckt_end, 'DD-Mon-YY') as time_bckt_end
,case when trim(TOT_PATIENTS) = '' then NULL else cast(REPLACE(TOT_PATIENTS, ',', '') as decimal(22,7) ) end as TOT_PATIENTS
,case when trim(TOT_PATIENTS_CHANGE) = '' then NULL else cast(REPLACE(TOT_PATIENTS_CHANGE, '%', '') as decimal(22,7) ) end as TOT_PATIENTS_CHANGE
,case when trim(CLASS_SHARE) = '' then NULL else cast(REPLACE(CLASS_SHARE, '%', '') as decimal(22,7) ) end as CLASS_SHARE
,case when trim(BRAND_SHARE) = '' then NULL else cast(REPLACE(BRAND_SHARE, '%', '') as decimal(22,7) ) end as BRAND_SHARE

,case when (trim(CLASS_SHARE_CHANGE) = '-') or (TRIM(CLASS_SHARE_CHANGE) = '')
 then NULL else cast(REPLACE(CLASS_SHARE_CHANGE,'%', '') as decimal(22,7) )end as CLASS_SHARE_CHANGE

,case when (trim(BRAND_SHARE_CHANGE ) = '-') or (TRIM(BRAND_SHARE_CHANGE) = '')
then NULL else cast(REPLACE(BRAND_SHARE_CHANGE, '%', '') as decimal(22,7) ) end as BRAND_SHARE_CHANGE

,cast(REPLACE(class_total, ',', '') as decimal (22,7) ) as class_total
,cast(REPLACE(brand_total, ',', '') as decimal (22,7) ) as brand_total
 from eisai_prod_db.ldg_iqvia_report
 ;