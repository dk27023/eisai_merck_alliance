drop table if exists eisai_prod_db.ldg_iqvia_report;
create table eisai_prod_db.ldg_iqvia_report
(
Brand_ID varchar(255)
,Brand_name varchar(255)                                                                                                                                               
,Indication varchar(255)                                                                                 
,Line_of_Therapy varchar(255)                                                                           
,TIME_BCKT_CD varchar(255)   
,TIME_BCKT_START varchar(255)       
,TIME_BCKT_END varchar(255)     
,TOT_PATIENTS varchar(255)
,TOT_PATIENTS_CHANGE varchar(255)
,CLASS_SHARE varchar(255)
,BRAND_SHARE varchar(255)
,CLASS_SHARE_CHANGE varchar(255)
,BRAND_SHARE_CHANGE varchar(255)                                
,class_total varchar(255)
,brand_total varchar(255)
);

TRUNCATE table eisai_prod_db.ldg_iqvia_report;

copy eisai_prod_db.ldg_iqvia_report 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/Dev/IQVIA_Report/12 Jan Data/IQVIA Report Sample data V2.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT AS CSV
IGNOREHEADER 1
;


select * from eisai_prod_db.ldg_iqvia_report;