create table eisai_prod_db.i_button_metric_iqvia as 
select distinct 
'TOTAL PATIENTS' as metric, 'Total number of Patients for that therapy/indication' as description
union 
select distinct 'CLASS SHARE', 'Share of the particular class for given time period'
union select distinct 'BRAND SHARE', 'Share of the particular brand for given time period'
union select distinct 'TOTAL PATIENTS CHANGE','Change in the number of patients for that thrapy/indication'
union select distinct 'CLASS SHARE CHANGE', 'Change in the class share for the given time period'
union select distinct 'BRAND SHARE CHANGE', 'Change in the brand share for the given time period';